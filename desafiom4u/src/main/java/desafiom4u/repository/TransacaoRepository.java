package desafiom4u.repository;

import java.util.Hashtable;


import java.util.Map;

import org.springframework.stereotype.Repository;

import desafiom4u.vo.TransacaoVO;
/**  
 * Classe responsavel fazer a integracao com a base de dados  
 */
@Repository
public class TransacaoRepository {
	
	public TransacaoRepository() {
		
	}
	
		
	private Map<String, TransacaoVO> repositorio = new Hashtable<String, TransacaoVO>();
		
		
	public void inserirTransacao(TransacaoVO transacao){
		repositorio.put(transacao.getTid(), transacao);
	}
	
	public TransacaoVO consultaTransacao(String tid){
		return repositorio.get(tid);
	}

}
