package desafiom4u.configuracao;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

import desafiom4u.adaptor.bandeirax.BandeiraXAdaptor;
/**
 * 
 * Classe que configura os beans que serao usados para as chamadas ws/SOAP para a BandeiraX
 *
 */
@Configuration
public class BandeiraXConfiguracao {

		
	@Bean
	public Jaxb2Marshaller marshaller() {
		Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
		marshaller.setContextPaths("desafiom4u.ws.contrato.bandeirax.autorizacao","desafiom4u.ws.contrato.bandeirax.common","desafiom4u.ws.contrato.bandeirax.cancelamento","desafiom4u.ws.contrato.bandeirax.consulta");
		Resource schema1 = new ClassPathResource("objetos_comuns.xsd");
		Resource schema2 = new ClassPathResource("operacao_autorizacao.xsd");
		Resource schema3 = new ClassPathResource("operacao_cancelamento.xsd");
		Resource schema4 = new ClassPathResource("operacao_consulta.xsd");
		Resource schema5 = new ClassPathResource("transacao_consulta.xsd");
		Resource schema6 = new ClassPathResource("transacao_request.xsd");
		Resource schema7 = new ClassPathResource("transacao_response.xsd");
		marshaller.setSchemas(schema1,schema2,schema3,schema4,schema5,schema6,schema7);
		return marshaller;
	}

	@Bean
	public BandeiraXAdaptor bandeiraXClient(Jaxb2Marshaller marshaller) {
		BandeiraXAdaptor client = new BandeiraXAdaptor();
		client.setDefaultUri("http://localhost:8089/ws");
		client.setMarshaller(marshaller);
		client.setUnmarshaller(marshaller);		
		return client;
	}

}
