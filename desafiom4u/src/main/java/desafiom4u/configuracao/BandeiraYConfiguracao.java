package desafiom4u.configuracao;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Scope;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
/**
 * 
 * Classe que configura os beans que serao usados para as chamadas ws/REST para a BandeiraY. 
 * Também mapeia os componentes que serao necessarios 
 */
@Configuration
@ComponentScan({"desafiom4u.adaptor.bandeiray"})
public class BandeiraYConfiguracao {
	
	
//	@Bean
//	@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
//	public BandeiraYAdaptor getInstance() {
//		return new BandeiraYAdaptor();
//	}

	@Bean
	@Primary
	@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
	@Autowired
	public MappingJackson2HttpMessageConverter objectMapperBuilder(ObjectMapper objectMapper) {
		MappingJackson2HttpMessageConverter builder = new MappingJackson2HttpMessageConverter();
		builder.setObjectMapper(objectMapper);
		builder.setPrettyPrint(false);
		return builder;
	}

	@Bean
	@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
	public ObjectMapper myObjectMapper() {
		ObjectMapper mapper = new ObjectMapper();
		
		mapper.enable(SerializationFeature.WRAP_ROOT_VALUE);		
		mapper.enable(DeserializationFeature.UNWRAP_ROOT_VALUE);
		mapper.disable(SerializationFeature.INDENT_OUTPUT);
		mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX"));
		

		return mapper;
	}

	@Bean
	@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
	public RestTemplate createRestTemplate(MappingJackson2HttpMessageConverter jsonMessageConverter) {
		RestTemplate restTemplate = new RestTemplate();
		List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
		messageConverters.add(jsonMessageConverter);
		restTemplate.setMessageConverters(messageConverters);

		return restTemplate;
	}
}
