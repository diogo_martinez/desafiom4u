package desafiom4u.configuracao;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
/**
 * 
 * Mapeia os componentes necessarios para o funcionamento do ws/REST para a BandeiraY
 *
 */
@Configuration
@ComponentScan({"desafiom4u.ws.endpoint.bandeiray.autorizacao"})
public class EndpointRESTCofiguracao {
	
	
	
}
