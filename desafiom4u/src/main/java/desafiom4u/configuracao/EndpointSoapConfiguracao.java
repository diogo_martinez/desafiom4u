package desafiom4u.configuracao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.ServletRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.ws.config.annotation.EnableWs;
import org.springframework.ws.server.EndpointInterceptor;
import org.springframework.ws.server.endpoint.mapping.PayloadRootAnnotationMethodEndpointMapping;
import org.springframework.ws.soap.server.endpoint.interceptor.PayloadValidatingInterceptor;
import org.springframework.ws.transport.http.MessageDispatcherServlet;
import org.springframework.ws.wsdl.wsdl11.DefaultWsdl11Definition;
import org.springframework.xml.xsd.SimpleXsdSchema;
import org.springframework.xml.xsd.XsdSchema;


/**
 * 
 * Mapeia os componentes necessarios para o funcionamento do ws/SOAP para a BandeiraX
 * Configura o interceptador de validacao do Payloader de requisicao
 * configura os dados de geracao do wsdl e servlets de mapeamento
 *
 */
@EnableWs
@Configuration
@ComponentScan({"desafiom4u.ws.endpoint.bandeirax.autorizacao"})
public class EndpointSoapConfiguracao {
	
	@Autowired
	private PayloadRootAnnotationMethodEndpointMapping endpointMapping;
	
	@Bean
	public ServletRegistrationBean messageDispatcherServlet(ApplicationContext applicationContext) {
		MessageDispatcherServlet servlet = new MessageDispatcherServlet();
		servlet.setApplicationContext(applicationContext);
		servlet.setTransformWsdlLocations(true);
		return new ServletRegistrationBean(servlet, "/desafio/ws/*");
	}

	@Bean(name = "desafio")
	public DefaultWsdl11Definition defaultWsdl11Definition(XsdSchema schema) {
		DefaultWsdl11Definition wsdl11Definition = new DefaultWsdl11Definition();
		wsdl11Definition.setPortTypeName("DesafioPort");
		wsdl11Definition.setLocationUri("/desafio/ws");
		wsdl11Definition.setTargetNamespace("autorizacao.bandeirax.contrato.endpoint.ws.desafiom4u");
		wsdl11Definition.setSchema(schema);
		return wsdl11Definition;
	}

	@Bean
	public XsdSchema respostaAutorizacaoSchema() {
		return new SimpleXsdSchema(new ClassPathResource("operacao_resposta_autorizacao.xsd"));
		}
	

	@Bean
	public PayloadValidatingInterceptor  validationInterceptor() {
	        final PayloadValidatingInterceptor  payloadValidatingInterceptor = new PayloadValidatingInterceptor ();
	        payloadValidatingInterceptor.setSchema(new ClassPathResource(
	                "operacao_resposta_autorizacao.xsd"));
	        addInterceptor(payloadValidatingInterceptor);
	        return payloadValidatingInterceptor;
	    }

	private void addInterceptor(PayloadValidatingInterceptor payloadValidatingInterceptor) {
		EndpointInterceptor[] interceptors = endpointMapping.getInterceptors();
		if (interceptors!= null && interceptors.length>0) {
			interceptors[interceptors.length]=payloadValidatingInterceptor;
		} else {
			 interceptors = new EndpointInterceptor[1];
			 interceptors[0]=payloadValidatingInterceptor;
		}
		
		endpointMapping.setInterceptors(interceptors);		
	}
	
	
}
