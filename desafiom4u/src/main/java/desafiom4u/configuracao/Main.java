package desafiom4u.configuracao;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 
 * Classe de inicializacao do sistema
 *
 */

@SpringBootApplication 
public class Main {

	public static void main(String[] args) {
		SpringApplication.run(Main.class);

	}

}
