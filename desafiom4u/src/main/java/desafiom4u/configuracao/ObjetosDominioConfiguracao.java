package desafiom4u.configuracao;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * 
 * mapeia os componentes do dominio da aplicacao
 *
 */

@Configuration
@ComponentScan({"desafiom4u.service","desafiom4u.repository","desafiom4u.factory"})
public class ObjetosDominioConfiguracao {

	
	
}
