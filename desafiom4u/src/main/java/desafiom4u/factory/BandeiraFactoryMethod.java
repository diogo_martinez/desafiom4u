package desafiom4u.factory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import desafiom4u.adaptor.BandeiraAdaptor;
import desafiom4u.adaptor.bandeirax.BandeiraXAdaptor;
import desafiom4u.adaptor.bandeiray.BandeiraYAdaptor;

/**  
 * Classe responsavel por criar a bandeira concreta a partir de parametros pre-definidos  
 */
@Component
public class BandeiraFactoryMethod {
	@Autowired	
	private  BandeiraYAdaptor bandeiraY;
	@Autowired
	private  BandeiraXAdaptor bandeiraX;

	public BandeiraAdaptor createBandeira(char bandeira)
			throws IllegalArgumentException {
		switch (bandeira) {
		case 'X':
			return bandeiraX;

		case 'Y':
			return bandeiraY;

		default:
			throw new IllegalArgumentException("Bandeira nao encontrada");

		}
	}

}
