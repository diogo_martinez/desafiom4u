package desafiom4u.helper;

import org.springframework.util.Assert;

import desafiom4u.vo.TransacaoVO;
import desafiom4u.ws.contrato.bandeirax.common.EnumStatusCancelamento;

public class TransacaoHelper {

	public static void validaInputAutorizacaoECancelamento(TransacaoVO transacaoVO) {
		Assert.notNull(transacaoVO.getTid());
		Assert.notNull(transacaoVO.getNumeroCartao());
		Assert.notNull(transacaoVO.getDataHora());
		Assert.notNull(transacaoVO.getValor());
		Assert.notNull(transacaoVO.getDescricao());
		Assert.notNull(transacaoVO.getModalidade());
		Assert.notNull(transacaoVO.getParcelas());
	}

	public static void validaRespostaAutorizacao(TransacaoVO transacaoVO) {
		Assert.notNull(transacaoVO.getTid());
		Assert.notNull(transacaoVO.getDataHora());
		Assert.notNull(transacaoVO.getValor());
		Assert.notNull(transacaoVO.getDescricao());
		Assert.notNull(transacaoVO.getModalidade());
		Assert.notNull(transacaoVO.getParcelas());
		if (transacaoVO.getStatusPagamento().equals("AUTORIZADO")
				|| transacaoVO.getStatusPagamento().equals("AUTORIZADA")) {
			Assert.notNull(transacaoVO.getNsu());
			Assert.notNull(transacaoVO.getCodAutorizacao());
		} else {
			Assert.isNull(transacaoVO.getNsu());
			Assert.isNull(transacaoVO.getCodAutorizacao());
		}
		Assert.notNull(transacaoVO.getStatusPagamento());

	}

	public static void validaRespostaCancelamento(TransacaoVO transacaoVO) {
		Assert.notNull(transacaoVO.getTid());
		Assert.notNull(transacaoVO.getDataHora());
		Assert.notNull(transacaoVO.getValor());
		Assert.notNull(transacaoVO.getDescricao());
		Assert.notNull(transacaoVO.getModalidade());
		Assert.notNull(transacaoVO.getParcelas());
		if (transacaoVO.getStatusCancelamento() == EnumStatusCancelamento.AUTORIZADO) {
			Assert.notNull(transacaoVO.getNsu());
			Assert.notNull(transacaoVO.getCodAutorizacao());
		} else {
			Assert.isNull(transacaoVO.getNsu());
			Assert.isNull(transacaoVO.getCodAutorizacao());
		}
		Assert.notNull(transacaoVO.getStatusCancelamento());

	}

	public static void validaRespostaConsulta(TransacaoVO transacaoVO) {
		Assert.notNull(transacaoVO.getTid());
		Assert.notNull(transacaoVO.getNumeroCartao());
		Assert.notNull(transacaoVO.getDataHora());
		Assert.notNull(transacaoVO.getValor());
		Assert.notNull(transacaoVO.getDescricao());
		Assert.notNull(transacaoVO.getModalidade());
		Assert.notNull(transacaoVO.getParcelas());
		Assert.notNull(transacaoVO.getNsu());
		Assert.notNull(transacaoVO.getCodAutorizacao());
		if (transacaoVO.getStatusCancelamento() == null) {
			Assert.notNull(transacaoVO.getStatusPagamento());
		} else {
			Assert.notNull(transacaoVO.getStatusCancelamento());
		}
	}

}
