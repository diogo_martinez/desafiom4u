package desafiom4u.service;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.CannotCreateTransactionException;

import desafiom4u.adaptor.BandeiraAdaptor;
import desafiom4u.factory.BandeiraFactoryMethod;
import desafiom4u.repository.TransacaoRepository;
import desafiom4u.vo.TransacaoVO;
import desafiom4u.ws.contrato.bandeirax.common.EnumStatusCancelamento;

/**
 * Classe responsavel por tratar da comunicacao e armazenamento das transacoes
 * recebidas do cliente
 */
@Service
public class TransacaoService {

	@Autowired
	private TransacaoRepository repositorio;

	@Autowired
	private BandeiraFactoryMethod factory;
	private static final Log logger = LogFactory.getLog(TransacaoService.class);

	/**
	 * atualiza a transacao de autorizacao
	 * 
	 * @param transacao
	 *            dados a serem enviados
	 * @throws IllegalArgumentException
	 *             se a transacao nao puder ser encontrada
	 * 
	 */
	public void atualizaAutorizacao(TransacaoVO transacaoVO) throws IllegalArgumentException {

		try {
			TransacaoVO transacao = repositorio.consultaTransacao(transacaoVO.getTid());
			if (transacao != null) {
				if (transacaoVO.getStatusPagamento().equals("AUTORIZADO")) {
					transacao.setCodAutorizacao(transacaoVO.getCodAutorizacao());
					transacao.setNsu(transacaoVO.getNsu());
				}
				logger.info("transacao nao autorizada: tid= " + transacao.getTid());

				transacao.setStatusPagamento(transacaoVO.getStatusPagamento());

			} else {
				logger.warn("Transacao com tid = " + transacaoVO.getTid() + " nao foi encontrada");
				throw new IllegalArgumentException("Transacao nao encontrada em nossa base de dados");
			}
		} catch (IllegalArgumentException e) {
			throw e;
		}

	}

	/**
	 * faz a o envio e armazenamanto da autorizacao de forma sincrona
	 * 
	 * @param transacao
	 *            dados a serem enviados
	 * @param bandeira
	 *            identificador da bandeira
	 * @throws IllegalArgumentException
	 *             <ul>
	 *             <li>se algum dado obrigatorio de requisicao ou resposta nao
	 *             foi informado ou ser informado com formato invalido
	 *             <li>se a o parametro de indentificacao da bandeira for
	 *             invalido
	 * @throws NullPointerException
	 *             se tiver erro de parse de dados
	 * 
	 * @throws IllegalStateException
	 *             se ocorrer falha de comunicacao com a bandeira
	 * @throws CannotCreateTransactionException
	 *             se a autorizacao for negada
	 * 
	 */
	public void autorizaSincrono(TransacaoVO transacaoVO, char bandeira) throws IllegalArgumentException {
		try {
			BandeiraAdaptor bandeiraAdaptor = factory.createBandeira(bandeira);
			TransacaoVO transacao = bandeiraAdaptor.autorizaSinc(transacaoVO);

			if (transacao.getStatusPagamento().equals("AUTORIZADO")) {
				transacao.setNumeroCartao(transacaoVO.getNumeroCartao());
				repositorio.inserirTransacao(transacao);
			} else {
				logger.info("transacao nao autorizada: tid= " + transacao.getTid());
				throw new CannotCreateTransactionException("transacao negada para o id : " + transacao.getTid());
			}

		} catch (NullPointerException e) {
			logger.error("Dado obrigatorio de requisicao/resposta nao foi informado");
			throw new IllegalArgumentException();
		} catch (IllegalArgumentException e) {
			logger.error("Dado obrigatorio de requisicao/resposta foi informado com formato invalido");
			throw e;
		} catch (IllegalStateException e) {
			logger.error("Erro na comunicacao com a bandeira: " + e.getMessage());
			throw e;
		} catch (CannotCreateTransactionException e) {
			logger.error("transacao negada");
			throw e;
		}
	}

	/**
	 * faz a o envio e armazenamanto da autorizacao de forma assincrona
	 * 
	 * @param transacao
	 *            dados a serem enviados
	 * @param bandeira
	 *            identificador da bandeira
	 * @throws IllegalArgumentException
	 *             <ul>
	 *             <li>se algum dado obrigatorio de requisicao ou resposta nao
	 *             foi informado ou ser informado com formato invalido
	 *             <li>se a o parametro de indentificacao da bandeira for
	 *             invalido
	 * @throws NullPointerException
	 *             se tiver erro de parse de dados
	 * 
	 * @throws IllegalStateException
	 *             se ocorrer falha de comunicacao com a bandeira
	 * 
	 */
	public void autorizaAssincrono(TransacaoVO transacaoVO, char bandeira) throws IllegalArgumentException {
		try {
			BandeiraAdaptor bandeiraAdaptor = factory.createBandeira(bandeira);
			bandeiraAdaptor.autorizaASinc(transacaoVO);
			repositorio.inserirTransacao(transacaoVO);

		} catch (NullPointerException e) {
			logger.error("Dado obrigatorio de requisicao/resposta nao foi informado");
			throw new IllegalArgumentException();
		} catch (IllegalArgumentException e) {
			logger.error("Dado obrigatorio de requisicao/resposta foi informado com formato invalido");
			throw e;
		} catch (IllegalStateException e) {
			logger.error("Erro na comunicacao com a bandeira: " + e.getMessage());
			throw e;
		}
	}

	/**
	 * faz o cancelamento da transacao
	 * 
	 * @param transacao
	 *            dados a serem enviados
	 * @param bandeira
	 *            identificador da bandeira
	 * @throws IllegalArgumentException
	 *             <ul>
	 *             <li>se algum dado obrigatorio de requisicao ou resposta nao
	 *             foi informado ou ser informado com formato invalido
	 *             <li>se a o parametro de indentificacao da bandeira for
	 *             invalido
	 * @throws NullPointerException
	 *             <ul>
	 *             <li>se tiver erro de parse de dados
	 *             <li>se a transacao nao for encontrada na base de dados
	 * @throws IllegalStateException
	 *             se ocorrer falha de comunicacao com a bandeira
	 * @throws CannotCreateTransactionException
	 *             se o cancelamento for negado
	 * 
	 */
	public void cancelamento(TransacaoVO transacaoVO, char bandeira) throws IllegalArgumentException {
		try {
			BandeiraAdaptor bandeiraAdaptor = factory.createBandeira(bandeira);
			TransacaoVO transacao = bandeiraAdaptor.cancela(transacaoVO);
			TransacaoVO transacaoPersistida = repositorio.consultaTransacao(transacao.getTid());

			if (transacao.getStatusCancelamento() == EnumStatusCancelamento.AUTORIZADO) {
				transacaoPersistida.setStatusCancelamento(transacao.getStatusCancelamento());
				transacaoPersistida.setStatusPagamento(null);
				transacaoPersistida.setNsu(transacao.getNsu());
				transacaoPersistida.setCodAutorizacao(transacao.getCodAutorizacao());
			} else {
				logger.info("cancelamento nao autorizado: tid= " + transacao.getTid());
				throw new CannotCreateTransactionException("cancelamento negado para o id : " + transacao.getTid());
			}
		} catch (NullPointerException e) {
			logger.error("Dado obrigatorio de requisicao/resposta nao foi informado ou a transacao nao existe na base");
			throw new IllegalArgumentException();
		} catch (IllegalArgumentException e) {
			logger.error("Dado obrigatorio de requisicao/resposta foi informado com formato invalido");
			throw e;
		} catch (IllegalStateException e) {
			logger.error("Erro na comunicacao com a bandeira: " + e.getMessage());
			throw e;
		} catch (CannotCreateTransactionException e) {
			logger.error("cancelamento negado");
			throw e;
		}
	}

	/**
	 * cosulta os dados das transacoes
	 * 
	 * @param tids
	 *            lista de tids a serem consultados
	 * @param bandeira
	 *            identificador da bandeira
	 * @throws IllegalArgumentException
	 *             <ul>
	 *             <li>se algum dado obrigatorio de requisicao ou resposta nao
	 *             foi informado ou ser informado com formato invalido
	 *             <li>se a o parametro de indentificacao da bandeira for
	 *             invalido
	 * @throws NullPointerException
	 *             se tiver erro de parse de dados
	 * 
	 * @throws IllegalStateException
	 *             se ocorrer falha de comunicacao com a bandeira
	 * 
	 */
	public List<TransacaoVO> consulta(List<String> tids, char bandeira) throws IllegalArgumentException {
		BandeiraAdaptor bandeiraAdaptor = factory.createBandeira(bandeira);

		List<TransacaoVO> transacoes = null;
		try {
			transacoes = bandeiraAdaptor.consulta(tids);
		} catch (NullPointerException e) {
			logger.error("Dado obrigatorio de requisicao/resposta nao foi informado");
			throw new IllegalArgumentException();
		} catch (IllegalArgumentException e) {
			logger.error("Dado obrigatorio de requisicao/resposta foi informado com formato invalido");
			throw e;
		} catch (IllegalStateException e) {
			logger.error("Erro na comunicacao com a bandeira: " + e.getMessage());
			throw e;
		}
		return transacoes;
	}

}
