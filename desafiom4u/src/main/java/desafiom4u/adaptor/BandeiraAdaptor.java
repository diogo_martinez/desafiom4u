package desafiom4u.adaptor;

import java.util.List;

import desafiom4u.vo.TransacaoVO;
/** * 
 * Interface base para disponibilizar a comunicacao com bandeiras 
 */
public interface BandeiraAdaptor {
	/**
	 * faz a o envio de autorizacao de forma sincrona
	 * 
	 * @param transacao
	 * 			dados a serem enviados
	 * @return
	 * 			resposta da Bandeira
	 */
	public TransacaoVO autorizaSinc(TransacaoVO transacao);
	/** 
	 * inicia a autorizacao assincrona
	 * 	  
	 * @param transacao
	 * 			dados a serem enviados
	 */
	public void autorizaASinc(TransacaoVO transacao);
	/**
	 * faz o cancelamento da transacao
	 * 
	 * @param transacao
	 * 			dados a serem enviados
	 * @return
	 * 			resposta da Bandeira
	 */
	public TransacaoVO cancela(TransacaoVO transacao);
	/**
	 * faz a consulta das transacoes
	 * 
	 * @param tids
	 * 			lista com todos os tids a serem consultados
	 * @return
	 * 			transacoes recebidas da Bandeira
	 */
	public List<TransacaoVO> consulta(List<String> tids);
	

}
