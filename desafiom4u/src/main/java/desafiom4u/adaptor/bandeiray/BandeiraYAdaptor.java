package desafiom4u.adaptor.bandeiray;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import desafiom4u.adaptor.BandeiraAdaptor;
import desafiom4u.helper.TransacaoHelper;
import desafiom4u.vo.TransacaoVO;
import desafiom4u.ws.contrato.bandeirax.common.EnumStatusCancelamento;
import desafiom4u.ws.contrato.bandeiray.DadosPedido;
import desafiom4u.ws.contrato.bandeiray.EnumStatusPagamento;
import desafiom4u.ws.contrato.bandeiray.FormaPagamento;
import desafiom4u.ws.contrato.bandeiray.Transacao;

/**
 * 
 * Classe que implementa a comunicacao com a BandeiraY
 */

@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class BandeiraYAdaptor implements BandeiraAdaptor {

	@Autowired
	private RestTemplate restTemplate;

	private static Logger logger = Logger.getLogger(BandeiraYAdaptor.class);

	@Override
	public TransacaoVO autorizaSinc(TransacaoVO transacaoVO) {

		Transacao transacaoRequest = parseToTransacaoAutorizacao(transacaoVO);
		Transacao transacaoResponse = null;
		try {
			transacaoResponse = restTemplate.postForObject("http://localhost:8089/rest", transacaoRequest,
					Transacao.class);
		} catch (HttpClientErrorException e) {
			logger.error("falha na comunicacao com a bandeira: " + e.getMessage());
			throw new IllegalStateException();
		}

		return parseToTransacaoVO(transacaoResponse);
	}

	private TransacaoVO parseToTransacaoVO(Transacao transacaoResponse) {

		TransacaoVO transacaoVO = new TransacaoVO();

		transacaoVO.setDataHora(transacaoResponse.getDadosPedido().getDataHora());
		transacaoVO.setDescricao(transacaoResponse.getDadosPedido().getDescricao());
		if (transacaoResponse.getDadosPedido() != null
				&& transacaoResponse.getDadosPedido().getStatusPagamento() == EnumStatusPagamento.AUTORIZADO) {
			transacaoVO.setCodAutorizacao(transacaoResponse.getDadosPedido().getCodigoAutorizacao());
			transacaoVO.setNsu(transacaoResponse.getDadosPedido().getNsu());
		}
		transacaoVO.setStatusPagamento(transacaoResponse.getDadosPedido().getStatusPagamento().value());
		transacaoVO.setTid(transacaoResponse.getTid());
		transacaoVO.setValor(transacaoResponse.getDadosPedido().getValor());
		transacaoVO.setParcelas(transacaoResponse.getFormaPagamento().getQuantidade());
		transacaoVO.setModalidade(transacaoResponse.getFormaPagamento().getTipo());

		TransacaoHelper.validaRespostaAutorizacao(transacaoVO);

		return transacaoVO;
	}

	private Transacao parseToTransacaoAutorizacao(TransacaoVO transacaoVO) {

		Transacao transacaoRequest = new Transacao();

		transacaoRequest.setTid(transacaoVO.getTid());

		DadosPedido dadosPedido = new DadosPedido();
		dadosPedido.setNumeroCartao(transacaoVO.getNumeroCartao());
		dadosPedido.setDataHora(transacaoVO.getDataHora());
		dadosPedido.setValor(transacaoVO.getValor());
		dadosPedido.setDescricao(transacaoVO.getDescricao());

		FormaPagamento formaPagamento = new FormaPagamento();
		formaPagamento.setTipo(transacaoVO.getModalidade());
		formaPagamento.setQuantidade(transacaoVO.getParcelas());

		transacaoRequest.setDadosPedido(dadosPedido);
		transacaoRequest.setFormaPagamento(formaPagamento);

		return transacaoRequest;
	}

	@Override
	public void autorizaASinc(TransacaoVO transacaoVO) {
		Transacao request = parseToTransacaoCancelamento(transacaoVO);
		try {
			restTemplate.postForObject("http://localhost:8089/rest/cancelamento", request, Transacao.class);
		} catch (HttpClientErrorException e) {
			logger.error("falha na comunicacao com a bandeira: " + e.getMessage());
			throw new IllegalStateException();
		}

	}

	@Override
	public TransacaoVO cancela(TransacaoVO transacaoVO) {
		Transacao request = parseToTransacaoCancelamento(transacaoVO);
		Transacao transacaoResponse = null;
		try {
			transacaoResponse = restTemplate.postForObject("http://localhost:8089/rest/cancelamento", request,
					Transacao.class);
		} catch (HttpClientErrorException e) {
			logger.error("falha na comunicacao com a bandeira: " + e.getMessage());
			throw new IllegalStateException();
		}
		return parseCancelamentoResponseToTransacaoVO(transacaoResponse);

	}

	private TransacaoVO parseCancelamentoResponseToTransacaoVO(Transacao transacaoResponse) {

		TransacaoVO transacaoVO = new TransacaoVO();

		transacaoVO.setDataHora(transacaoResponse.getDadosPedido().getDataHora());
		transacaoVO.setDescricao(transacaoResponse.getDadosPedido().getDescricao());
		if (transacaoResponse.getDadosPedido() != null
				&& transacaoResponse.getDadosPedido().getStatusCancelamento() == EnumStatusCancelamento.AUTORIZADO) {
			transacaoVO.setNsu(transacaoResponse.getDadosPedido().getNsu());
			transacaoVO.setCodAutorizacao(transacaoResponse.getDadosPedido().getCodigoAutorizacao());
		}
		transacaoVO.setStatusCancelamento(transacaoResponse.getDadosPedido().getStatusCancelamento());
		transacaoVO.setTid(transacaoResponse.getTid());
		transacaoVO.setValor(transacaoResponse.getDadosPedido().getValor());
		transacaoVO.setParcelas(transacaoResponse.getFormaPagamento().getParcelas());
		transacaoVO.setModalidade(transacaoResponse.getFormaPagamento().getModalidade());

		TransacaoHelper.validaRespostaCancelamento(transacaoVO);

		return transacaoVO;

	}

	private Transacao parseToTransacaoCancelamento(TransacaoVO transacaoVO) {
		Transacao transacaoRequest = new Transacao();

		DadosPedido dadosPedido = new DadosPedido();
		FormaPagamento formaPagamento = new FormaPagamento();

		formaPagamento.setTipo(transacaoVO.getModalidade());
		formaPagamento.setQuantidade(transacaoVO.getParcelas());

		dadosPedido.setDataHora(transacaoVO.getDataHora());
		dadosPedido.setDescricao(transacaoVO.getDescricao());
		dadosPedido.setNumeroCartao(transacaoVO.getNumeroCartao());
		dadosPedido.setValor(transacaoVO.getValor());
		transacaoRequest.setDadosPedido(dadosPedido);
		transacaoRequest.setFormaPagamento(formaPagamento);
		transacaoRequest.setTid(transacaoVO.getTid());

		return transacaoRequest;

	}

	@Override
	public List<TransacaoVO> consulta(List<String> tids) {
		// o codigo so possui um message converter mas, por seguranca, ja que
		// alguem pode incluir um novo converter futuramente, efetuar uma busca
		// nos
		// objetos mapeados
		for (Object object : restTemplate.getMessageConverters()) {
			if (object instanceof MappingJackson2HttpMessageConverter) {
				MappingJackson2HttpMessageConverter converter = (MappingJackson2HttpMessageConverter) object;
				ObjectMapper mapper = converter.getObjectMapper();
				mapper.disable(DeserializationFeature.UNWRAP_ROOT_VALUE);
				break;
			}
		}
		Transacao[] transacaoResponse = null;
		try {
			transacaoResponse = restTemplate.getForObject("http://localhost:8089/rest/{tids}", Transacao[].class, tids);
		} catch (HttpClientErrorException e) {
			logger.error("falha na comunicacao com a bandeira: " + e.getMessage());
			throw new IllegalStateException();
		}
		return parseToListTransacaoVO(transacaoResponse);
	}

	private List<TransacaoVO> parseToListTransacaoVO(Transacao[] transacaoResponse) {
		List<TransacaoVO> listaTransacaoVO = new ArrayList<TransacaoVO>();
		for (Transacao transacao : transacaoResponse) {
			TransacaoVO transacaoVO = new TransacaoVO();
			transacaoVO.setCodAutorizacao(transacao.getDadosPedido().getCodigoAutorizacao());
			transacaoVO.setDataHora(transacao.getDadosPedido().getDataHora());
			transacaoVO.setDescricao(transacao.getDadosPedido().getDescricao());
			transacaoVO.setNsu(transacao.getDadosPedido().getNsu().intValue());
			transacaoVO.setNumeroCartao(transacao.getDadosPedido().getNumeroCartao());
			if (transacao.getDadosPedido().getStatusPagamento() != null) {
				transacaoVO.setStatusPagamento(transacao.getDadosPedido().getStatusPagamento().value());
			}
			transacaoVO.setStatusCancelamento(transacao.getDadosPedido().getStatusCancelamento());
			transacaoVO.setTid(transacao.getTid());
			transacaoVO.setValor(transacao.getDadosPedido().getValor().intValue());
			transacaoVO.setParcelas(transacao.getFormaPagamento().getParcelas());
			transacaoVO.setModalidade(transacao.getFormaPagamento().getModalidade());
			TransacaoHelper.validaRespostaConsulta(transacaoVO);
			listaTransacaoVO.add(transacaoVO);
		}
		return listaTransacaoVO;
	}

}
