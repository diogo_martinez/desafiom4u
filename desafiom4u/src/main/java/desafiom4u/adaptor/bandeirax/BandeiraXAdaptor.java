package desafiom4u.adaptor.bandeirax;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;

import org.apache.log4j.Logger;
import org.springframework.oxm.MarshallingFailureException;
import org.springframework.oxm.UnmarshallingFailureException;
import org.springframework.ws.client.WebServiceTransportException;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;

import desafiom4u.adaptor.BandeiraAdaptor;
import desafiom4u.vo.TransacaoVO;
import desafiom4u.ws.contrato.bandeirax.autorizacao.AutorizacaoRequest;
import desafiom4u.ws.contrato.bandeirax.autorizacao.AutorizacaoResponse;
import desafiom4u.ws.contrato.bandeirax.autorizacao.DadosPedidoRequest;
import desafiom4u.ws.contrato.bandeirax.autorizacao.TransacaoRequest;
import desafiom4u.ws.contrato.bandeirax.cancelamento.CancelamentoRequest;
import desafiom4u.ws.contrato.bandeirax.cancelamento.CancelamentoResponse;
import desafiom4u.ws.contrato.bandeirax.common.EnumStatusCancelamento;
import desafiom4u.ws.contrato.bandeirax.common.EnumStatusPagamento;
import desafiom4u.ws.contrato.bandeirax.common.FormaPagamento;
import desafiom4u.ws.contrato.bandeirax.consulta.ConsultaRequest;
import desafiom4u.ws.contrato.bandeirax.consulta.ConsultaResponse;
import desafiom4u.ws.contrato.bandeirax.consulta.TransacaoConsulta;
import desafiom4u.ws.contrato.bandeirax.consulta.Transacoes;

/**
 * * Classe que implementa a comunicacao com a BandeiraX
 */

public class BandeiraXAdaptor extends WebServiceGatewaySupport implements BandeiraAdaptor {

	private static Logger logger = Logger.getLogger(BandeiraXAdaptor.class);

	@Override
	public TransacaoVO autorizaSinc(TransacaoVO transacao) {

		AutorizacaoRequest request = null;
		try {
			request = parseToAutorizacaoRequest(transacao);
		} catch (DatatypeConfigurationException e) {
			logger.error("erro na trasformacao da data para o tipo XMLGregorianCalendar");
			throw new IllegalArgumentException();
		}
		AutorizacaoResponse response = null;
		try {
			response = (AutorizacaoResponse) getWebServiceTemplate().marshalSendAndReceive("http://localhost:8089/ws",
					request, new SoapActionCallback("http://localhost:8089/ws/AutorizacaoRequest"));
		} catch (MarshallingFailureException e) {
			logger.error("erro na operacao de marshall: " + e.getMessage());
			throw new IllegalArgumentException();
		} catch (WebServiceTransportException e) {
			logger.error("falha na comunicacao com a bandeira: " + e.getMessage());
			throw new IllegalStateException();
		} catch (UnmarshallingFailureException e) {
			logger.error("erro na operacao de unmarshall: " + e.getMessage());
			throw new IllegalArgumentException();
		}

		return parseAutorizacaoResponseToTransacaoVO(response);
	}

	private TransacaoVO parseAutorizacaoResponseToTransacaoVO(AutorizacaoResponse response) {
		TransacaoVO transacaoVO = new TransacaoVO();
		if (response.getTransacao().getDadosPedido().getStatusPagamento() == EnumStatusPagamento.AUTORIZADA) {
			transacaoVO.setCodAutorizacao(response.getTransacao().getDadosPedido().getCodigoAutorizacao().intValue());
			transacaoVO.setNsu(response.getTransacao().getDadosPedido().getNsu().intValue());
		}
		transacaoVO.setDataHora(response.getTransacao().getDadosPedido().getDataHora().toGregorianCalendar());
		transacaoVO.setDescricao(response.getTransacao().getDadosPedido().getDescricao());

		transacaoVO.setStatusPagamento(response.getTransacao().getDadosPedido().getStatusPagamento().value());
		transacaoVO.setTid(response.getTransacao().getTid());
		transacaoVO.setValor(response.getTransacao().getDadosPedido().getValor().intValue());
		transacaoVO.setParcelas(response.getTransacao().getFormaPagamento().getParcelas().intValue());
		transacaoVO.setModalidade(response.getTransacao().getFormaPagamento().getModalidade());
		return transacaoVO;
	}

	private AutorizacaoRequest parseToAutorizacaoRequest(TransacaoVO transacaoVO)
			throws DatatypeConfigurationException {
		AutorizacaoRequest autorizacaoRequest = new AutorizacaoRequest();
		TransacaoRequest transacaoRequest = new TransacaoRequest();
		DadosPedidoRequest dadosPedido = new DadosPedidoRequest();
		FormaPagamento formaPagamento = new FormaPagamento();

		formaPagamento.setModalidade(transacaoVO.getModalidade());
		formaPagamento.setParcelas(BigInteger.valueOf(transacaoVO.getParcelas()));

		GregorianCalendar gregorianCalendar = new GregorianCalendar();
		gregorianCalendar.setTimeInMillis(transacaoVO.getDataHora().getTimeInMillis());

		dadosPedido.setDataHora(DatatypeFactory.newInstance().newXMLGregorianCalendar(gregorianCalendar));
		dadosPedido.setDescricao(transacaoVO.getDescricao());
		dadosPedido.setNumeroCartao(transacaoVO.getNumeroCartao());
		dadosPedido.setValor(BigInteger.valueOf(transacaoVO.getValor()));
		transacaoRequest.setDadosPedido(dadosPedido);
		transacaoRequest.setFormaPagamento(formaPagamento);
		transacaoRequest.setTid(transacaoVO.getTid());

		autorizacaoRequest.setTransacao(transacaoRequest);

		return autorizacaoRequest;
	}

	@Override
	public void autorizaASinc(TransacaoVO transacao) {
		AutorizacaoRequest request = null;
		try {
			request = parseToAutorizacaoRequest(transacao);
		} catch (DatatypeConfigurationException e) {
			logger.error("erro na trasformacao da data para o tipo XMLGregorianCalendar");
			throw new IllegalArgumentException();
		}
		try {
			getWebServiceTemplate().marshalSendAndReceive("http://localhost:8089/ws", request,
					new SoapActionCallback("http://localhost:8089/ws/AutorizacaoAssincronaRequest"));
		} catch (MarshallingFailureException e) {
			logger.error("erro na operacao de marshall: " + e.getMessage());
			throw new IllegalArgumentException();
		} catch (WebServiceTransportException e) {
			logger.error("falha na comunicacao com a bandeira: " + e.getMessage());
			throw new IllegalArgumentException();
		} catch (UnmarshallingFailureException e) {
			logger.error("erro na operacao de unmarshall: " + e.getMessage());
			throw new IllegalArgumentException();
		}
	}

	@Override
	public TransacaoVO cancela(TransacaoVO transacao) {
		CancelamentoRequest request = null;
		try {
			request = parseToCancelamentoRequest(transacao);
		} catch (DatatypeConfigurationException e) {
			logger.error("erro na trasformacao da data para o tipo XMLGregorianCalendar");
			throw new IllegalArgumentException();
		}
		CancelamentoResponse response = null;
		try {

			response = (CancelamentoResponse) getWebServiceTemplate().marshalSendAndReceive("http://localhost:8089/ws",
					request, new SoapActionCallback("http://localhost:8089/ws/CancelamentoRequest"));
		} catch (MarshallingFailureException e) {
			logger.error("erro na operacao de marshall: " + e.getMessage());
			throw new IllegalArgumentException();
		} catch (WebServiceTransportException e) {
			logger.error("falha na comunicacao com a bandeira: " + e.getMessage());
			throw new IllegalStateException();
		} catch (UnmarshallingFailureException e) {
			logger.error("erro na operacao de unmarshall: " + e.getMessage());
			throw new IllegalArgumentException();
		}

		return parseCancelamentoResponseToTransacaoVO(response);

	}

	private TransacaoVO parseCancelamentoResponseToTransacaoVO(CancelamentoResponse response) {
		TransacaoVO transacaoVO = new TransacaoVO();
		if (response.getTransacao().getDadosPedido().getStatusCancelamento() == EnumStatusCancelamento.AUTORIZADO) {
			transacaoVO.setCodAutorizacao(response.getTransacao().getDadosPedido().getCodigoAutorizacao().intValue());
			transacaoVO.setNsu(response.getTransacao().getDadosPedido().getNsu().intValue());
		}
		
		transacaoVO.setDataHora(response.getTransacao().getDadosPedido().getDataHora().toGregorianCalendar());
		transacaoVO.setDescricao(response.getTransacao().getDadosPedido().getDescricao());
		
		transacaoVO.setStatusCancelamento(response.getTransacao().getDadosPedido().getStatusCancelamento());
		transacaoVO.setTid(response.getTransacao().getTid());
		transacaoVO.setValor(response.getTransacao().getDadosPedido().getValor().intValue());
		transacaoVO.setParcelas(response.getTransacao().getFormaPagamento().getParcelas().intValue());
		transacaoVO.setModalidade(response.getTransacao().getFormaPagamento().getModalidade());
		return transacaoVO;
	}

	private CancelamentoRequest parseToCancelamentoRequest(TransacaoVO transacaoVO)
			throws DatatypeConfigurationException {
		CancelamentoRequest cancelamentoRequest = new CancelamentoRequest();
		TransacaoRequest transacaoRequest = new TransacaoRequest();
		DadosPedidoRequest dadosPedido = new DadosPedidoRequest();
		FormaPagamento formaPagamento = new FormaPagamento();

		formaPagamento.setModalidade(transacaoVO.getModalidade());
		formaPagamento.setParcelas(BigInteger.valueOf(transacaoVO.getParcelas()));

		GregorianCalendar gregorianCalendar = new GregorianCalendar();
		gregorianCalendar.setTimeInMillis(transacaoVO.getDataHora().getTimeInMillis());

		dadosPedido.setDataHora(DatatypeFactory.newInstance().newXMLGregorianCalendar(gregorianCalendar));
		dadosPedido.setDescricao(transacaoVO.getDescricao());
		dadosPedido.setNumeroCartao(transacaoVO.getNumeroCartao());
		dadosPedido.setValor(BigInteger.valueOf(transacaoVO.getValor()));
		transacaoRequest.setDadosPedido(dadosPedido);
		transacaoRequest.setFormaPagamento(formaPagamento);
		transacaoRequest.setTid(transacaoVO.getTid());

		cancelamentoRequest.setTransacao(transacaoRequest);
		return cancelamentoRequest;
	}

	@Override
	public List<TransacaoVO> consulta(List<String> tids) {

		ConsultaRequest consultaRequest = new ConsultaRequest();
		consultaRequest.setTransacoes(new Transacoes());
		for (String string : tids) {
			consultaRequest.getTransacoes().getTid().add(string);
		}
		ConsultaResponse response = null;
		try {
			response = (ConsultaResponse) getWebServiceTemplate().marshalSendAndReceive("http://localhost:8089/ws",
					consultaRequest, new SoapActionCallback("http://localhost:8089/ws/ConsultaRequest"));
		} catch (MarshallingFailureException e) {
			logger.error("erro na operacao de marshall: " + e.getMessage());
			throw new IllegalArgumentException();
		} catch (WebServiceTransportException e) {
			logger.error("falha na comunicacao com a bandeira: " + e.getMessage());
			throw new IllegalStateException();
		} catch (UnmarshallingFailureException e) {
			logger.error("erro na operacao de unmarshall: " + e.getMessage());
			throw new IllegalArgumentException();
		}
		return parseConsultaResponseToTransacaoVO(response);
	}

	private List<TransacaoVO> parseConsultaResponseToTransacaoVO(ConsultaResponse response) {
		List<TransacaoVO> listaTransacaoVO = new ArrayList<TransacaoVO>();
		for (TransacaoConsulta transacaoConsulta : response.getTransacoes().getTransacao()) {
			TransacaoVO transacaoVO = new TransacaoVO();
			transacaoVO.setCodAutorizacao(transacaoConsulta.getDadosPedido().getCodigoAutorizacao().intValue());
			transacaoVO.setDataHora(transacaoConsulta.getDadosPedido().getDataHora().toGregorianCalendar());
			transacaoVO.setDescricao(transacaoConsulta.getDadosPedido().getDescricao());
			transacaoVO.setNsu(transacaoConsulta.getDadosPedido().getNsu().intValue());
			transacaoVO.setNumeroCartao(transacaoConsulta.getDadosPedido().getNumeroCartao());
			if (transacaoConsulta.getDadosPedido().getStatusPagamento() != null) {
				transacaoVO.setStatusPagamento(transacaoConsulta.getDadosPedido().getStatusPagamento().value());
			}
			transacaoVO.setStatusCancelamento(transacaoConsulta.getDadosPedido().getStatusCancelamento());
			transacaoVO.setTid(transacaoConsulta.getTid());
			transacaoVO.setValor(transacaoConsulta.getDadosPedido().getValor().intValue());
			transacaoVO.setParcelas(transacaoConsulta.getFormaPagamento().getParcelas().intValue());
			transacaoVO.setModalidade(transacaoConsulta.getFormaPagamento().getModalidade());
			listaTransacaoVO.add(transacaoVO);
		}
		return listaTransacaoVO;
	}

}
