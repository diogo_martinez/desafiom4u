package desafiom4u.ws.contrato.bandeiray;

import java.util.Calendar;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import desafiom4u.ws.contrato.bandeirax.common.EnumStatusCancelamento;


@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class DadosPedido {


	private String numeroCartao;
	private Integer valor;
	
	@JsonDeserialize(as=Calendar.class )
	@JsonProperty(value = "data-hora")
	private Calendar dataHora;
	private String descricao;
	
	private Integer nsu;
	
	private Integer codigoAutorizacao;
	@JsonProperty(value="status")
	@NotNull
	private EnumStatusPagamento statusPagamento;
	private EnumStatusCancelamento statusCancelamento;
	public String getNumeroCartao() {
		return numeroCartao;
	}
	public void setNumeroCartao(String numeroCartao) {
		this.numeroCartao = numeroCartao;
	}
	public Integer getValor() {
		return valor;
	}
	public void setValor(Integer valor) {
		this.valor = valor;
	}
	
	public Calendar getDataHora() {
		return dataHora;
	}
	
	
	public void setDataHora(Calendar dataHora) {
		this.dataHora = dataHora;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public Integer getNsu() {
		return nsu;
	}
	public void setNsu(Integer nsu) {
		this.nsu = nsu;
	}
	public Integer getCodigoAutorizacao() {
		return codigoAutorizacao;
	}
	public void setCodigoAutorizacao(Integer codigoAutorizacao) {
		this.codigoAutorizacao = codigoAutorizacao;
	}
	public EnumStatusPagamento getStatusPagamento() {
		return statusPagamento;
	}
	public void setStatusPagamento(EnumStatusPagamento statusPagamento) {
		this.statusPagamento = statusPagamento;
	}
	public EnumStatusCancelamento getStatusCancelamento() {
		return statusCancelamento;
	}
	public void setStatusCancelamento(EnumStatusCancelamento statusCancelamento) {
		this.statusCancelamento = statusCancelamento;
	}
	@Override
	public String toString() {
		return "DadosPedido [numeroCartao=" + numeroCartao + ", valor=" + valor
				+ ", dataHora=" + dataHora + ", descricao=" + descricao
				+ ", nsu=" + nsu + ", codigoAutorizacao=" + codigoAutorizacao
				+ ", statusPagamento=" + statusPagamento
				+ ", statusCancelamento=" + statusCancelamento + "]";
	}
}
