

package desafiom4u.ws.contrato.bandeiray;

import com.fasterxml.jackson.annotation.JsonCreator;

public enum EnumStatusPagamento {

    AUTORIZADO("AUTORIZADO"),
    
    NEGADA_SALDO("NEGADA SALDO"),
    
    NAO_AUTORIZADO("NAO AUTORIZADO");
    private final String value;

    EnumStatusPagamento(String v) {
        value = v;
    }

    public String value() {
        return value;
    }
    @JsonCreator
    public static EnumStatusPagamento fromValue(String v) {
        for (EnumStatusPagamento c: EnumStatusPagamento.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
   
}
