package desafiom4u.ws.contrato.bandeiray;

import javax.validation.Valid;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

@JsonRootName(value ="transacao")
public class Transacao {
	
	@NotEmpty
	private String tid;
	
	@JsonProperty("forma-pagamento")	
	private FormaPagamento formaPagamento;
	
	@JsonProperty("dados-pedido")
	@Valid
	private DadosPedido dadosPedido;
	
	public String getTid() {
		return tid;
	}

	public void setTid(String tid) {
		this.tid = tid;
	}	

	public FormaPagamento getFormaPagamento() {
		return formaPagamento;
	}

	public void setFormaPagamento(FormaPagamento formaPagamento) {
		this.formaPagamento = formaPagamento;
	}

	public DadosPedido getDadosPedido() {
		return dadosPedido;
	}

	public void setDadosPedido(DadosPedido dadosPedido) {
		this.dadosPedido = dadosPedido;
	}

	@Override
	public String toString() {
		return "Transacao [tid=" + tid + ", formaPagamento=" + formaPagamento
				+ ", dadosPedido=" + dadosPedido + "]";
	}

}
