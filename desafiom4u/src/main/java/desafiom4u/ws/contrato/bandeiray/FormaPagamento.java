
package desafiom4u.ws.contrato.bandeiray;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import desafiom4u.ws.contrato.bandeirax.common.EnumModalidade;

@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class FormaPagamento {


    private EnumModalidade modalidade;


    private Integer parcelas;
    
    
    private EnumModalidade tipo;
    
    
    private Integer quantidade;

    public EnumModalidade getModalidade() {
        return modalidade;
    }


    public void setModalidade(EnumModalidade value) {
        this.modalidade = value;
    }

    public Integer getParcelas() {
        return parcelas;
    }

    public void setParcelas(Integer value) {
        this.parcelas = value;
    }


	public EnumModalidade getTipo() {
		return tipo;
	}


	public void setTipo(EnumModalidade tipo) {
		this.tipo = tipo;
	}


	public Integer getQuantidade() {
		return quantidade;
	}


	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}


	@Override
	public String toString() {
		return "FormaPagamento [modalidade=" + modalidade + ", parcelas="
				+ parcelas + ", tipo=" + tipo + ", quantidade=" + quantidade
				+ "]";
	}

}
