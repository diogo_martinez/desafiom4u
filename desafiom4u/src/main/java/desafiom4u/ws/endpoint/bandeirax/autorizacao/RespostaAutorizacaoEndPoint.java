package desafiom4u.ws.endpoint.bandeirax.autorizacao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import desafiom4u.helper.TransacaoHelper;
import desafiom4u.service.TransacaoService;
import desafiom4u.vo.TransacaoVO;
import desafiom4u.ws.contrato.bandeirax.common.EnumModalidade;
import desafiom4u.ws.endpoint.contrato.bandeirax.autorizacao.EnumStatusPagamento;
import desafiom4u.ws.endpoint.contrato.bandeirax.autorizacao.RespostaAutorizacaoRequest;

/**
 * 
 * Endpoint ws/SOAP para receber as respostas de autorizacao da BandeiraX
 *
 */
@Endpoint
public class RespostaAutorizacaoEndPoint {

	private static final String NAMESPACE_URI = "autorizacao.bandeirax.contrato.endpoint.ws.desafiom4u";

	@Autowired
	private TransacaoService transacaoService;

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "RespostaAutorizacaoRequest")
	@ResponsePayload
	public void respostaAutorizacao(@RequestPayload RespostaAutorizacaoRequest request) {
		TransacaoVO transacaoVO = parseToTransacaoVO(request);
		try {
			transacaoService.atualizaAutorizacao(transacaoVO);
		}catch (Exception e) {
			throw e;
		}

	}

	private TransacaoVO parseToTransacaoVO(RespostaAutorizacaoRequest request) {
		TransacaoVO transacaoVO = new TransacaoVO();
		if(request.getTransacao().getDadosPedido().getStatusPagamento() == EnumStatusPagamento.AUTORIZADA){
		transacaoVO.setCodAutorizacao(request.getTransacao().getDadosPedido().getCodigoAutorizacao().intValue());
		transacaoVO.setNsu(request.getTransacao().getDadosPedido().getNsu().intValue());
		}
		transacaoVO.setDataHora(request.getTransacao().getDadosPedido().getDataHora().toGregorianCalendar());
		transacaoVO.setDescricao(request.getTransacao().getDadosPedido().getDescricao());
		
		transacaoVO.setStatusPagamento(request.getTransacao().getDadosPedido().getStatusPagamento().value());
		transacaoVO.setTid(request.getTransacao().getTid());
		transacaoVO.setValor(request.getTransacao().getDadosPedido().getValor().intValue());
		transacaoVO.setParcelas(request.getTransacao().getFormaPagamento().getParcelas().intValue());
		transacaoVO.setModalidade(
				EnumModalidade.fromValue(request.getTransacao().getFormaPagamento().getModalidade().value()));
		
		TransacaoHelper.validaRespostaAutorizacao(transacaoVO);
		
		return transacaoVO;
	}
}
