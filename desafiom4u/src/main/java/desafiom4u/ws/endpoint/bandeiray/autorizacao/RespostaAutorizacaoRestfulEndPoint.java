package desafiom4u.ws.endpoint.bandeiray.autorizacao;

import javax.validation.Valid;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import desafiom4u.helper.TransacaoHelper;
import desafiom4u.service.TransacaoService;
import desafiom4u.vo.TransacaoVO;
import desafiom4u.ws.contrato.bandeiray.EnumStatusPagamento;
import desafiom4u.ws.contrato.bandeiray.Transacao;

/**
 * 
 * Endpoint ws/REST para receber as respostas de autorizacao da BandeiraY
 *
 */
@RestController
public class RespostaAutorizacaoRestfulEndPoint {

	@Autowired
	private TransacaoService transacaoService;

	private static final Log logger = LogFactory.getLog(RespostaAutorizacaoRestfulEndPoint.class);

	/**
	 * endpoint resposta autorizacao
	 * 
	 * @param transacao
	 *          dados da trasacao a serem atualizados
	 * @param results
	 *			erros de validacao de dados. A validacao esta mapeada apenas
	 *          para os dados que identificam a transacao e que ainda nao
	 *          existem na base
	 * @return
	 *			mensagem do processamento e status http
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/desafio/rest/respostaAutorizacao", consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<String> respostaAutorizacao(@Valid @RequestBody Transacao transacao, BindingResult results) {
		

		if (results.hasErrors()) {
			logger.error(results.getAllErrors());
			return new ResponseEntity<String>("dados invalidos", HttpStatus.BAD_REQUEST);

		} else {

			try {
				TransacaoVO transacaoVO = parseToTransacaoVO(transacao);
				transacaoService.atualizaAutorizacao(transacaoVO);			
				
			} catch (IllegalArgumentException e) {
				return new ResponseEntity<String>("transacao nao encontrada", HttpStatus.BAD_REQUEST);
			} catch (Exception e) {
				return new ResponseEntity<String>("Falha no processamento dos dados", HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}
		return new ResponseEntity<String>("ok", HttpStatus.OK);

	}

	private TransacaoVO parseToTransacaoVO(Transacao request) {
		TransacaoVO transacaoVO = new TransacaoVO();

		transacaoVO.setDataHora(request.getDadosPedido().getDataHora());
		transacaoVO.setDescricao(request.getDadosPedido().getDescricao());
		if (request.getDadosPedido() != null
				&& request.getDadosPedido().getStatusPagamento() == EnumStatusPagamento.AUTORIZADO) {
			transacaoVO.setCodAutorizacao(request.getDadosPedido().getCodigoAutorizacao());
			transacaoVO.setNsu(request.getDadosPedido().getNsu());
		}
		transacaoVO.setStatusPagamento(request.getDadosPedido().getStatusPagamento().value());
		transacaoVO.setTid(request.getTid());
		transacaoVO.setValor(request.getDadosPedido().getValor());
		transacaoVO.setParcelas(request.getFormaPagamento().getQuantidade());
		transacaoVO.setModalidade(request.getFormaPagamento().getTipo());
		
		TransacaoHelper.validaRespostaAutorizacao(transacaoVO);
		

		return transacaoVO;
	}

}
