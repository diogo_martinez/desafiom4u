//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.7 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2015.11.23 at 09:32:21 PM BRST 
//


package desafiom4u.ws.endpoint.contrato.bandeirax.autorizacao;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for EnumModalidade.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="EnumModalidade">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="AVISTA"/>
 *     &lt;enumeration value="PARCELADO LOJA"/>
 *     &lt;enumeration value="PARCELADO ADM"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "EnumModalidade")
@XmlEnum
public enum EnumModalidade {

    AVISTA("AVISTA"),
    @XmlEnumValue("PARCELADO LOJA")
    PARCELADO_LOJA("PARCELADO LOJA"),
    @XmlEnumValue("PARCELADO ADM")
    PARCELADO_ADM("PARCELADO ADM");
    private final String value;

    EnumModalidade(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static EnumModalidade fromValue(String v) {
        for (EnumModalidade c: EnumModalidade.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
