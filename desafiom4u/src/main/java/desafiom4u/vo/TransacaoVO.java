package desafiom4u.vo;

import java.util.Calendar;

import desafiom4u.ws.contrato.bandeirax.common.EnumModalidade;
import desafiom4u.ws.contrato.bandeirax.common.EnumStatusCancelamento;
/**
 * 
 * POJO para trafegar os dados da transacao nas camadas de dominio 
 *
 */
public class TransacaoVO {
	
	private String tid;
	private Integer codAutorizacao;
	private Integer nsu;
	private Calendar dataHora;
	private String descricao;
	private Integer valor;
	private String statusPagamento;
	private EnumStatusCancelamento statusCancelamento;
	private EnumModalidade modalidade;
	private Integer parcelas;
	private String numeroCartao;
	
	public TransacaoVO(){
		
	}
	public TransacaoVO(String tid, Integer codAutorizacao, Integer nsu, Calendar dataHora, String descricao,
			Integer valor, String statusPagamento, EnumStatusCancelamento statusCancelamento,
			EnumModalidade modalidade, Integer parcelas, String numeroCartao) {
		super();
		this.tid = tid;
		this.codAutorizacao = codAutorizacao;
		this.nsu = nsu;
		this.dataHora = dataHora;
		this.descricao = descricao;
		this.valor = valor;
		this.statusPagamento = statusPagamento;
		this.statusCancelamento = statusCancelamento;
		this.modalidade = modalidade;
		this.parcelas = parcelas;
		this.numeroCartao = numeroCartao;
	}
	public String getTid() {
		return tid;
	}
	public void setTid(String tid) {
		this.tid = tid;
	}
	public Integer getCodAutorizacao() {
		return codAutorizacao;
	}
	public void setCodAutorizacao(Integer codAutorizacao) {
		this.codAutorizacao = codAutorizacao;
	}
	public Integer getNsu() {
		return nsu;
	}
	public void setNsu(Integer nsu) {
		this.nsu = nsu;
	}
	public Calendar getDataHora() {
		return dataHora;
	}
	public void setDataHora(Calendar dataHora) {
		this.dataHora = dataHora;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public Integer getValor() {
		return valor;
	}
	public void setValor(Integer valor) {
		this.valor = valor;
	}
	public String getStatusPagamento() {
		return statusPagamento;
	}
	public void setStatusPagamento(String statusPagamento) {
		this.statusPagamento = statusPagamento;
	}
	public EnumStatusCancelamento getStatusCancelamento() {
		return statusCancelamento;
	}
	public void setStatusCancelamento(EnumStatusCancelamento statusCancelamento) {
		this.statusCancelamento = statusCancelamento;
	}
	public EnumModalidade getModalidade() {
		return modalidade;
	}
	public void setModalidade(EnumModalidade modalidade) {
		this.modalidade = modalidade;
	}
	public Integer getParcelas() {
		return parcelas;
	}
	public void setParcelas(Integer parcelas) {
		this.parcelas = parcelas;
	}
	public String getNumeroCartao() {
		return numeroCartao;
	}
	public void setNumeroCartao(String numeroCartao) {
		this.numeroCartao = numeroCartao;
	}
	@Override
	public String toString() {
		return "TransacaoVO [tid=" + tid + ", codAutorizacao=" + codAutorizacao
				+ ", nsu=" + nsu + ", dataHora=" + dataHora + ", descricao="
				+ descricao + ", valor=" + valor + ", statusPagamento="
				+ statusPagamento + ", statusCancelamento="
				+ statusCancelamento + ", modalidade=" + modalidade
				+ ", parcelas=" + parcelas + ", numeroCartao=" + numeroCartao
				+ "]";
	}

}
