package desafiom4u;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.transaction.CannotCreateTransactionException;

import desafiom4u.adaptor.BandeiraAdaptor;
import desafiom4u.factory.BandeiraFactoryMethod;
import desafiom4u.repository.TransacaoRepository;
import desafiom4u.service.TransacaoService;
import desafiom4u.vo.TransacaoVO;
import desafiom4u.ws.contrato.bandeirax.common.EnumModalidade;
import desafiom4u.ws.contrato.bandeirax.common.EnumStatusCancelamento;




public class IntegraServicoERepositorioIntegracaoTest {
	
		
	@InjectMocks
	TransacaoService service = new TransacaoService();
	
	@Spy
	TransacaoRepository repositorio = new TransacaoRepository();
	@Mock
	BandeiraFactoryMethod factory;
	
	@Mock
	BandeiraAdaptor bandeira;
	@Mock
	private TransacaoVO transacaoVO;
	
		
	@Before
	public void setUp(){
		
		MockitoAnnotations.initMocks(this);
		repositorio.inserirTransacao(new TransacaoVO("1", null, null, Calendar.getInstance(), "teste", 100, null, null, EnumModalidade.AVISTA, 1,
				"1234567890"));
		repositorio.inserirTransacao(new TransacaoVO("2", null, null, Calendar.getInstance(), "teste", 100, null, null, EnumModalidade.AVISTA, 1,
				"1234567890"));
		repositorio.inserirTransacao(new TransacaoVO("3", 2342332, 1234345, Calendar.getInstance(), "teste", 100, "AUTORIZADO", null, EnumModalidade.AVISTA, 1,
				"1234567890"));
	}
	@Test
	public void testaAutorizacaoSincrona(){
		TransacaoVO transacaoVO = new TransacaoVO("1", null, null, Calendar.getInstance(), "teste", 100, null, null, EnumModalidade.AVISTA, 1,
				"1234567890");
		TransacaoVO transacaoRetornoAut = new TransacaoVO("1", 989898, 234321, Calendar.getInstance(), "teste", 100, "AUTORIZADO", null, EnumModalidade.AVISTA, 1,
				null);
		when(factory.createBandeira('X')).thenReturn(bandeira);
		when(bandeira.autorizaSinc(transacaoVO)).thenReturn(transacaoRetornoAut);
		service.autorizaSincrono(transacaoVO, 'X');
		assertNotNull(repositorio.consultaTransacao("1"));
		assertNotNull(repositorio.consultaTransacao("1").getCodAutorizacao());
		assertNotNull(repositorio.consultaTransacao("1").getStatusPagamento());
		assertNotNull(repositorio.consultaTransacao("1").getNsu());
		assertNull(repositorio.consultaTransacao("1").getStatusCancelamento());
	}
	
	@Test(expected = CannotCreateTransactionException.class)
	public void testaAutorizacaoSincronaNegada(){
		TransacaoVO transacaoVO = new TransacaoVO("1", null, null, Calendar.getInstance(), "teste", 100, null, null, EnumModalidade.AVISTA, 1,
				"1234567890");
		TransacaoVO transacaoRetornoAut = new TransacaoVO("1", null, null, Calendar.getInstance(), "teste", 100, "NAO AUTORIZADO", null, EnumModalidade.AVISTA, 1,
				null);
		when(factory.createBandeira('X')).thenReturn(bandeira);
		when(bandeira.autorizaSinc(transacaoVO)).thenReturn(transacaoRetornoAut);
		service.autorizaSincrono(transacaoVO, 'X');
		assertNotNull(repositorio.consultaTransacao("1"));
		assertNull(repositorio.consultaTransacao("1").getCodAutorizacao());
		assertNull(repositorio.consultaTransacao("1").getStatusPagamento());
		assertNull(repositorio.consultaTransacao("1").getNsu());
		assertEquals(repositorio.consultaTransacao("1").getStatusPagamento(),"NAO AUTORIZADO");
		assertNull(repositorio.consultaTransacao("1").getStatusCancelamento());
	}
	
	@Test
	public void testaCancelamentoSucesso(){
		
		
		TransacaoVO transacaoRetornoCanc = new TransacaoVO("1", 989898, 234321, Calendar.getInstance(), "teste", 100, null, EnumStatusCancelamento.AUTORIZADO, EnumModalidade.AVISTA, 1,
					null);
		when(factory.createBandeira('X')).thenReturn(bandeira);
		when(bandeira.cancela(transacaoVO)).thenReturn(transacaoRetornoCanc);
		service.cancelamento(transacaoVO, 'X');
		assertNotNull(repositorio.consultaTransacao("1"));
		assertNotNull(repositorio.consultaTransacao("1").getCodAutorizacao());
		assertNull(repositorio.consultaTransacao("1").getStatusPagamento());
		assertNotNull(repositorio.consultaTransacao("1").getNsu());
		assertNotNull(repositorio.consultaTransacao("1").getStatusCancelamento());
	}
	
	@Test
	public void testaCancelamentoNegado(){
				
		TransacaoVO transacaoRetornoCanc = new TransacaoVO("3", null, null, Calendar.getInstance(), "teste", 100, null, EnumStatusCancelamento.NEGADO, EnumModalidade.AVISTA, 1,
					null);
		when(factory.createBandeira('X')).thenReturn(bandeira);
		when(bandeira.cancela(transacaoVO)).thenReturn(transacaoRetornoCanc);
		try{
		service.cancelamento(transacaoVO, 'X');
		}catch (Exception e) {
			assertTrue(e instanceof CannotCreateTransactionException);
		}
		assertNotNull(repositorio.consultaTransacao("3"));
		assertNotNull(repositorio.consultaTransacao("3").getCodAutorizacao());
		assertNotNull(repositorio.consultaTransacao("3").getStatusPagamento());
		assertNotNull(repositorio.consultaTransacao("3").getNsu());
		assertNull(repositorio.consultaTransacao("3").getStatusCancelamento());
		assertEquals(repositorio.consultaTransacao("3").getStatusPagamento(),"AUTORIZADO");
	}
	
		
	@Test
	public void testeReqAutorizacaoASincrona(){
		TransacaoVO transacaoVO = new TransacaoVO("2", null, null, Calendar.getInstance(), "teste", 100, null, null, EnumModalidade.AVISTA, 1,
				"1234567890");		
		when(factory.createBandeira('X')).thenReturn(bandeira);
		doNothing().when(bandeira).autorizaASinc(transacaoVO);
		service.autorizaAssincrono(transacaoVO, 'X');
		assertNotNull(repositorio.consultaTransacao("2"));
		assertNull(repositorio.consultaTransacao("2").getCodAutorizacao());		
		assertNull(repositorio.consultaTransacao("2").getStatusPagamento());
		assertNull(repositorio.consultaTransacao("2").getNsu());
		assertNull(repositorio.consultaTransacao("2").getStatusCancelamento());
		assertNotNull(repositorio.consultaTransacao("2").getTid());
		assertNotNull(repositorio.consultaTransacao("2").getNumeroCartao());
		assertNotNull(repositorio.consultaTransacao("2").getDataHora());
		assertNotNull(repositorio.consultaTransacao("2").getDescricao());
	}
	@Test
	public void testeResAutorizacaoASincrona(){
		
		
		TransacaoVO transacaoRetornoAut = new TransacaoVO("2", 989898, 234321, Calendar.getInstance(), "teste", 100, "AUTORIZADO", null, EnumModalidade.AVISTA, 1,
				null);		
		service.atualizaAutorizacao(transacaoRetornoAut);
		assertNotNull(repositorio.consultaTransacao("2"));
		assertNotNull(repositorio.consultaTransacao("2").getCodAutorizacao());
		assertNotNull(repositorio.consultaTransacao("2").getStatusPagamento());
		assertNotNull(repositorio.consultaTransacao("2").getNsu());
		assertNull(repositorio.consultaTransacao("2").getStatusCancelamento());
	}
	
	@Test
	public void testeResAutorizacaoASincronaNegada(){
		
		
		TransacaoVO transacaoRetornoAut = new TransacaoVO("2", null, null, Calendar.getInstance(), "teste", 100, "NAO AUTORIZADO", null, EnumModalidade.AVISTA, 1,
				null);		
		service.atualizaAutorizacao(transacaoRetornoAut);
		assertNotNull(repositorio.consultaTransacao("2"));
		assertNull(repositorio.consultaTransacao("2").getCodAutorizacao());
		assertNotNull(repositorio.consultaTransacao("2").getStatusPagamento());
		assertNull(repositorio.consultaTransacao("2").getNsu());
		assertNull(repositorio.consultaTransacao("2").getStatusCancelamento());
		assertEquals(repositorio.consultaTransacao("2").getStatusPagamento(),"NAO AUTORIZADO");
	}
	
	@Test
	public void testeConsulta(){
		when(factory.createBandeira('X')).thenReturn(bandeira);
		when(bandeira.consulta(Arrays.asList("1"))).thenReturn(new ArrayList<TransacaoVO>());
			
		service.consulta(Arrays.asList("1"), 'X');
		
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testeAutorizacaoSincronaNullPointer(){
		when(factory.createBandeira('X')).thenReturn(bandeira);
		when(bandeira.autorizaSinc(transacaoVO)).thenThrow(new NullPointerException());
			
		service.autorizaSincrono(transacaoVO, 'X');
		
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testeAutorizacaoSincronaIllegalArgumet(){
		when(factory.createBandeira('X')).thenReturn(bandeira);
		when(bandeira.autorizaSinc(transacaoVO)).thenThrow(new IllegalArgumentException());
			
		service.autorizaSincrono(transacaoVO, 'X');
		
	}
	@Test(expected = IllegalStateException.class)
	public void testeAutorizacaoSincronaIllegalState(){
		when(factory.createBandeira('X')).thenReturn(bandeira);
		when(bandeira.autorizaSinc(transacaoVO)).thenThrow(new IllegalStateException());
			
		service.autorizaSincrono(transacaoVO, 'X');
		
	}
	@Test(expected = RuntimeException.class)
	public void testeAutorizacaoSincronaRunTime(){
		when(factory.createBandeira('X')).thenReturn(bandeira);
		when(bandeira.autorizaSinc(transacaoVO)).thenThrow(new RuntimeException());
			
		service.autorizaSincrono(transacaoVO, 'X');
		
	}
	@Test(expected = IllegalArgumentException.class)
	public void testeAutorizacaoASincronaNullPointer(){
		when(factory.createBandeira('X')).thenReturn(bandeira);
		doThrow(new NullPointerException()).when(bandeira).autorizaASinc(transacaoVO);
			
		service.autorizaAssincrono(transacaoVO, 'X');
		
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testeAutorizacaoASincronaIllegalArgumet(){
		when(factory.createBandeira('X')).thenReturn(bandeira);
		doThrow(new IllegalArgumentException()).when(bandeira).autorizaASinc(transacaoVO);
			
		service.autorizaAssincrono(transacaoVO, 'X');
		
	}
	@Test(expected = IllegalStateException.class)
	public void testeAutorizacaoASincronaIllegalState(){
		when(factory.createBandeira('X')).thenReturn(bandeira);
		doThrow(new IllegalStateException()).when(bandeira).autorizaASinc(transacaoVO);
			
		service.autorizaAssincrono(transacaoVO, 'X');
		
	}
	@Test(expected = RuntimeException.class)
	public void testeAutorizacaoASincronaRunTime(){
		when(factory.createBandeira('X')).thenReturn(bandeira);
		doThrow(new RuntimeException()).when(bandeira).autorizaASinc(transacaoVO);
			
		service.autorizaAssincrono(transacaoVO, 'X');
		
	}
	@Test(expected = IllegalArgumentException.class)
	public void testeCancelamentoNullPointer(){
		when(factory.createBandeira('X')).thenReturn(bandeira);
		when(bandeira.cancela(transacaoVO)).thenThrow(new NullPointerException());
			
		service.cancelamento(transacaoVO, 'X');
		
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testeCancelamentoIllegalArgumet(){
		when(factory.createBandeira('X')).thenReturn(bandeira);
		when(bandeira.autorizaSinc(transacaoVO)).thenThrow(new IllegalArgumentException());
			
		service.cancelamento(transacaoVO, 'X');
		
	}
	@Test(expected = IllegalStateException.class)
	public void testeCancelamentoIllegalState(){
		when(factory.createBandeira('X')).thenReturn(bandeira);
		when(bandeira.cancela(transacaoVO)).thenThrow(new IllegalStateException());
			
		service.cancelamento(transacaoVO, 'X');
		
	}
	@Test(expected = RuntimeException.class)
	public void testeCancelamentoRunTime(){
		when(factory.createBandeira('X')).thenReturn(bandeira);
		when(bandeira.cancela(transacaoVO)).thenThrow(new RuntimeException());
			
		service.cancelamento(transacaoVO, 'X');
		
	}
	@Test(expected = IllegalArgumentException.class)
	public void testeConsultaNullPointer(){
		when(factory.createBandeira('X')).thenReturn(bandeira);
		when(bandeira.consulta(Arrays.asList(""))).thenThrow(new NullPointerException());
			
		service.consulta(Arrays.asList(""), 'X');
		
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testeConsulaIllegalArgumet(){
		when(factory.createBandeira('X')).thenReturn(bandeira);
		when(bandeira.consulta(Arrays.asList(""))).thenThrow(new IllegalArgumentException());
			
		service.consulta(Arrays.asList(""), 'X');
		
	}
	@Test(expected = IllegalStateException.class)
	public void testeConsultaIllegalState(){
		when(factory.createBandeira('X')).thenReturn(bandeira);
		when(bandeira.consulta(Arrays.asList(""))).thenThrow(new IllegalStateException());
			
		service.consulta(Arrays.asList(""), 'X');
		
	}
	@Test(expected = RuntimeException.class)
	public void testeConsultaRunTime(){
		when(factory.createBandeira('X')).thenReturn(bandeira);
		when(bandeira.consulta(Arrays.asList(""))).thenThrow(new RuntimeException());
			
		service.consulta(Arrays.asList(""), 'X');
		
	}

}
