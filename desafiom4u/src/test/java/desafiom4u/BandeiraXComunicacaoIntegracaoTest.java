package desafiom4u;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.equalToXml;
import static com.github.tomakehurst.wiremock.client.WireMock.post;
import static com.github.tomakehurst.wiremock.client.WireMock.stubFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;

import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.github.tomakehurst.wiremock.junit.WireMockClassRule;

import desafiom4u.adaptor.bandeirax.BandeiraXAdaptor;
import desafiom4u.configuracao.BandeiraXConfiguracao;
import desafiom4u.vo.TransacaoVO;
import desafiom4u.ws.contrato.bandeirax.common.EnumModalidade;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = BandeiraXConfiguracao.class)
public class BandeiraXComunicacaoIntegracaoTest {

	@ClassRule
	public static WireMockClassRule wireMock = new WireMockClassRule(8089);

	@Rule
	public WireMockClassRule instanceRule = wireMock;

	TransacaoVO transacaoVOSucesso;
	TransacaoVO transacaoVOErro;

	@Autowired
	BandeiraXAdaptor bandeiraXadaptor;
	
	@Before
	public void setUp() {
		SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
		Calendar c = Calendar.getInstance();
		try {
			c.setTime(fmt.parse("2015-11-12T10:10:31.471-02:00"));
		} catch (ParseException e) {
			e.printStackTrace();
		}

		transacaoVOSucesso = new TransacaoVO("1", null, null, c, "teste", 100, null, null, EnumModalidade.AVISTA, 3,
				"1234567890");
		transacaoVOErro = new TransacaoVO(null, null, null, c, "teste", 100, null, null, EnumModalidade.AVISTA, 3,
				"1234567890");
	}
	
	@Test
	public void testaAutorizacaoNegada() {
		StringBuilder response = new StringBuilder();
		response.append("<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\">");
		response.append("<SOAP-ENV:Header />");
		response.append("<SOAP-ENV:Body>");
		response.append("<ns2:AutorizacaoResponse xmlns:ns2=\"autorizacao.bandeirax.contrato.ws.desafiom4u\"");
		response.append("	xmlns:ns3=\"common.bandeirax.contrato.ws.desafiom4u\">");
		response.append("	<ns2:transacao>");
		response.append("		<ns2:tid>1</ns2:tid>");
		response.append("		<ns2:dados-pedido>");
		response.append("			<ns2:valor>100</ns2:valor>");
		response.append("			<ns2:dataHora>2015-11-12T10:10:31.471-02:00</ns2:dataHora>");
		response.append("			<ns2:descricao>teste</ns2:descricao>");
//		response.append("			<ns2:nsu>34324</ns2:nsu>");
//		response.append("			<ns2:codigoAutorizacao>234324</ns2:codigoAutorizacao>");
		response.append("			<ns2:statusPagamento>NAO AUTORIZADA</ns2:statusPagamento>");
		response.append("		</ns2:dados-pedido>");
		response.append("		<ns2:forma-pagamento>");
		response.append("			<ns3:modalidade>AVISTA</ns3:modalidade>");
		response.append("			<ns3:parcelas>3</ns3:parcelas>");
		response.append("		</ns2:forma-pagamento>");
		response.append("	</ns2:transacao>");
		response.append("</ns2:AutorizacaoResponse>");
		response.append("	</SOAP-ENV:Body>");
		response.append("</SOAP-ENV:Envelope>");

		stubFor(post(urlEqualTo("/ws"))
				.withRequestBody(equalToXml(
						"<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\"><SOAP-ENV:Header/><SOAP-ENV:Body><ns2:AutorizacaoRequest xmlns:ns2=\"autorizacao.bandeirax.contrato.ws.desafiom4u\" xmlns:ns3=\"common.bandeirax.contrato.ws.desafiom4u\" xmlns:ns4=\"cancelamento.bandeirax.contrato.ws.desafiom4u\" xmlns:ns5=\"consulta.bandeirax.contrato.ws.desafiom4u\"><ns2:transacao><ns2:tid>1</ns2:tid><ns2:dados-pedido><ns2:numeroCartao>1234567890</ns2:numeroCartao><ns2:valor>100</ns2:valor><ns2:dataHora>2015-11-12T10:10:31.471-02:00</ns2:dataHora><ns2:descricao>teste</ns2:descricao></ns2:dados-pedido><ns2:forma-pagamento><ns3:modalidade>AVISTA</ns3:modalidade><ns3:parcelas>3</ns3:parcelas></ns2:forma-pagamento></ns2:transacao></ns2:AutorizacaoRequest></SOAP-ENV:Body></SOAP-ENV:Envelope>"))
				.willReturn(aResponse().withStatus(200).withHeader("Content-Type", "text/xml")
						.withBody(response.toString())));

		TransacaoVO resposta = bandeiraXadaptor.autorizaSinc(this.transacaoVOSucesso);

		assertNull(resposta.getCodAutorizacao());
		assertNull(resposta.getNsu());
		assertNotNull(resposta.getStatusPagamento());
		assertNotNull(resposta.getTid());
		assertNotNull(resposta.getDescricao());
		assertNotNull(resposta.getModalidade());
		assertNotNull(resposta.getParcelas());
		assertNotNull(resposta.getValor());
		assertNotNull(resposta.getDataHora());

		SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
		String dataString = fmt.format(resposta.getDataHora().getTime());
		assertEquals("2015-11-12T10:10:31.471-02:00", dataString);

		assertNull(resposta.getNumeroCartao());
		assertNull(resposta.getStatusCancelamento());

	}


	@Test
	public void testaAutorizacaoSucesso() {
		StringBuilder response = new StringBuilder();
		response.append("<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\">");
		response.append("<SOAP-ENV:Header />");
		response.append("<SOAP-ENV:Body>");
		response.append("<ns2:AutorizacaoResponse xmlns:ns2=\"autorizacao.bandeirax.contrato.ws.desafiom4u\"");
		response.append("	xmlns:ns3=\"common.bandeirax.contrato.ws.desafiom4u\">");
		response.append("	<ns2:transacao>");
		response.append("		<ns2:tid>1</ns2:tid>");
		response.append("		<ns2:dados-pedido>");
		response.append("			<ns2:valor>100</ns2:valor>");
		response.append("			<ns2:dataHora>2015-11-12T10:10:31.471-02:00</ns2:dataHora>");
		response.append("			<ns2:descricao>teste</ns2:descricao>");
		response.append("			<ns2:nsu>123445566</ns2:nsu>");
		response.append("			<ns2:codigoAutorizacao>998766541</ns2:codigoAutorizacao>");
		response.append("			<ns2:statusPagamento>AUTORIZADA</ns2:statusPagamento>");
		response.append("		</ns2:dados-pedido>");
		response.append("		<ns2:forma-pagamento>");
		response.append("			<ns3:modalidade>AVISTA</ns3:modalidade>");
		response.append("			<ns3:parcelas>3</ns3:parcelas>");
		response.append("		</ns2:forma-pagamento>");
		response.append("	</ns2:transacao>");
		response.append("</ns2:AutorizacaoResponse>");
		response.append("	</SOAP-ENV:Body>");
		response.append("</SOAP-ENV:Envelope>");

		stubFor(post(urlEqualTo("/ws"))
				.withRequestBody(equalToXml(
						"<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\"><SOAP-ENV:Header/><SOAP-ENV:Body><ns2:AutorizacaoRequest xmlns:ns2=\"autorizacao.bandeirax.contrato.ws.desafiom4u\" xmlns:ns3=\"common.bandeirax.contrato.ws.desafiom4u\" xmlns:ns4=\"cancelamento.bandeirax.contrato.ws.desafiom4u\" xmlns:ns5=\"consulta.bandeirax.contrato.ws.desafiom4u\"><ns2:transacao><ns2:tid>1</ns2:tid><ns2:dados-pedido><ns2:numeroCartao>1234567890</ns2:numeroCartao><ns2:valor>100</ns2:valor><ns2:dataHora>2015-11-12T10:10:31.471-02:00</ns2:dataHora><ns2:descricao>teste</ns2:descricao></ns2:dados-pedido><ns2:forma-pagamento><ns3:modalidade>AVISTA</ns3:modalidade><ns3:parcelas>3</ns3:parcelas></ns2:forma-pagamento></ns2:transacao></ns2:AutorizacaoRequest></SOAP-ENV:Body></SOAP-ENV:Envelope>"))
				.willReturn(aResponse().withStatus(200).withHeader("Content-Type", "text/xml")
						.withBody(response.toString())));

		TransacaoVO resposta = bandeiraXadaptor.autorizaSinc(this.transacaoVOSucesso);

		assertNotNull(resposta.getCodAutorizacao());
		assertNotNull(resposta.getNsu());
		assertNotNull(resposta.getStatusPagamento());
		assertNotNull(resposta.getTid());
		assertNotNull(resposta.getDescricao());
		assertNotNull(resposta.getModalidade());
		assertNotNull(resposta.getParcelas());
		assertNotNull(resposta.getValor());
		assertNotNull(resposta.getDataHora());

		SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
		String dataString = fmt.format(resposta.getDataHora().getTime());
		assertEquals("2015-11-12T10:10:31.471-02:00", dataString);

		assertNull(resposta.getNumeroCartao());
		assertNull(resposta.getStatusCancelamento());

	}

	@Test(expected = IllegalArgumentException.class)
	public void testaAutorizacaoErroMarshalling() {

		bandeiraXadaptor.autorizaSinc(this.transacaoVOErro);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testaAutorizacaoErroUnmarshalling() {
		StringBuilder response = new StringBuilder();
		response.append("<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\">");
		response.append("<SOAP-ENV:Header />");
		response.append("<SOAP-ENV:Body>");
		response.append("<ns2:AutorizacaoResponse xmlns:ns2=\"autorizacao.bandeirax.contrato.ws.desafiom4u\"");
		response.append("	xmlns:ns3=\"common.bandeirax.contrato.ws.desafiom4u\">");
		response.append("	<ns2:transacao>");
		response.append("		<ns2:tid>1</ns2:tid>");
		response.append("		<ns2:dados-pedido>");
		response.append("			<ns2:valor>100</ns2:valor>");
		response.append("			<ns2:dataHora>2015-11-12T10:10:31.471-02:00</ns2:dataHora>");
		response.append("			<ns2:descricao>teste</ns2:descricao>");
		response.append("			<ns2:nsu>123445566</ns2:nsu>");
		response.append("			<ns2:codigoAutorizacao>998766541</ns2:codigoAutorizacao>");
		response.append("			<ns2:statusPagamento>AUTORIZADA</ns2:statusPagamento>");
		response.append("		</ns2:dados-pedido>");
		response.append("		<ns2:forma-pagamento>");
		response.append("			");
		response.append("			<ns3:parcelas>3</ns3:parcelas>");
		response.append("		</ns2:forma-pagamento>");
		response.append("	</ns2:transacao>");
		response.append("</ns2:AutorizacaoResponse>");
		response.append("	</SOAP-ENV:Body>");
		response.append("</SOAP-ENV:Envelope>");

		stubFor(post(urlEqualTo("/ws")).willReturn(
				aResponse().withStatus(200).withHeader("Content-Type", "text/xml").withBody(response.toString())));

		bandeiraXadaptor.autorizaSinc(this.transacaoVOSucesso);
	}

	@Test(expected = IllegalStateException.class)
	public void testaAutorizacaoErroConectividade() {

		stubFor(post(urlEqualTo("/ws")).willReturn(aResponse().withStatus(404)));

		bandeiraXadaptor.autorizaSinc(this.transacaoVOSucesso);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testaCancelamentoErroMarshalling() {

		bandeiraXadaptor.cancela(this.transacaoVOErro);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testaCancelamentoErroUnmarshalling() {
		StringBuilder response = new StringBuilder();
		response.append("<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\">");
		response.append("<SOAP-ENV:Header />");
		response.append("<SOAP-ENV:Body>");
		response.append("<ns2:AutorizacaoResponse xmlns:ns2=\"autorizacao.bandeirax.contrato.ws.desafiom4u\"");
		response.append("	xmlns:ns3=\"common.bandeirax.contrato.ws.desafiom4u\">");
		response.append("	<ns2:transacao>");
		response.append("		<ns2:tid>1</ns2:tid>");
		response.append("		<ns2:dados-pedido>");
		response.append("			<ns2:valor>100</ns2:valor>");
		response.append("			<ns2:dataHora>2015-11-12T10:10:31.471-02:00</ns2:dataHora>");
		response.append("			<ns2:descricao>teste</ns2:descricao>");
		response.append("			<ns2:nsu>123445566</ns2:nsu>");
		response.append("			<ns2:codigoAutorizacao>998766541</ns2:codigoAutorizacao>");
		response.append("			<ns2:statusPagamento>AUTORIZADA</ns2:statusPagamento>");
		response.append("		</ns2:dados-pedido>");
		response.append("		<ns2:forma-pagamento>");
		response.append("			");
		response.append("			<ns3:parcelas>3</ns3:parcelas>");
		response.append("		</ns2:forma-pagamento>");
		response.append("	</ns2:transacao>");
		response.append("</ns2:AutorizacaoResponse>");
		response.append("	</SOAP-ENV:Body>");
		response.append("</SOAP-ENV:Envelope>");

		stubFor(post(urlEqualTo("/ws")).willReturn(
				aResponse().withStatus(200).withHeader("Content-Type", "text/xml").withBody(response.toString())));

		bandeiraXadaptor.cancela(this.transacaoVOSucesso);
	}

	@Test(expected = IllegalStateException.class)
	public void testaCancelamentoErroConectividade() {

		stubFor(post(urlEqualTo("/ws")).willReturn(aResponse().withStatus(404)));

		bandeiraXadaptor.cancela(this.transacaoVOSucesso);
	}

	@Test
	public void testaCancelamentoSucesso() {
		StringBuilder response = new StringBuilder();
		response.append("<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\">");
		response.append("<SOAP-ENV:Header />");
		response.append("<SOAP-ENV:Body>");
		response.append("<ns2:CancelamentoResponse xmlns:ns2=\"cancelamento.bandeirax.contrato.ws.desafiom4u\"");
		response.append("	xmlns:ns3=\"common.bandeirax.contrato.ws.desafiom4u\">");
		response.append("	<ns2:transacao>");
		response.append("		<ns2:tid>1</ns2:tid>");
		response.append("		<ns2:dados-pedido>");
		response.append("			<ns2:valor>100</ns2:valor>");
		response.append("			<ns2:dataHora>2015-11-12T10:10:31.471-02:00</ns2:dataHora>");
		response.append("			<ns2:descricao>teste</ns2:descricao>");
		response.append("			<ns2:nsu>123445566</ns2:nsu>");
		response.append("			<ns2:codigoAutorizacao>998766541</ns2:codigoAutorizacao>");
		response.append("			<ns2:statusCancelamento>AUTORIZADO</ns2:statusCancelamento>");
		response.append("		</ns2:dados-pedido>");
		response.append("		<ns2:forma-pagamento>");
		response.append("			<ns3:modalidade>AVISTA</ns3:modalidade>");
		response.append("			<ns3:parcelas>3</ns3:parcelas>");
		response.append("		</ns2:forma-pagamento>");
		response.append("	</ns2:transacao>");
		response.append("</ns2:CancelamentoResponse>");
		response.append("	</SOAP-ENV:Body>");
		response.append("</SOAP-ENV:Envelope>");

		stubFor(post(urlEqualTo("/ws"))
				.withRequestBody(equalToXml(
						"<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\"><SOAP-ENV:Header/><SOAP-ENV:Body><ns4:CancelamentoRequest xmlns:ns2=\"autorizacao.bandeirax.contrato.ws.desafiom4u\" xmlns:ns3=\"common.bandeirax.contrato.ws.desafiom4u\" xmlns:ns4=\"cancelamento.bandeirax.contrato.ws.desafiom4u\" xmlns:ns5=\"consulta.bandeirax.contrato.ws.desafiom4u\"><ns4:transacao><ns2:tid>1</ns2:tid><ns2:dados-pedido><ns2:numeroCartao>1234567890</ns2:numeroCartao><ns2:valor>100</ns2:valor><ns2:dataHora>2015-11-12T10:10:31.471-02:00</ns2:dataHora><ns2:descricao>teste</ns2:descricao></ns2:dados-pedido><ns2:forma-pagamento><ns3:modalidade>AVISTA</ns3:modalidade><ns3:parcelas>3</ns3:parcelas></ns2:forma-pagamento></ns4:transacao></ns4:CancelamentoRequest></SOAP-ENV:Body></SOAP-ENV:Envelope>"))
				.willReturn(aResponse().withStatus(200).withHeader("Content-Type", "text/xml")
						.withBody(response.toString())));

		TransacaoVO resposta = bandeiraXadaptor.cancela(this.transacaoVOSucesso);
		assertNotNull(resposta.getCodAutorizacao());
		assertNotNull(resposta.getNsu());
		assertNotNull(resposta.getStatusCancelamento());
		assertNotNull(resposta.getTid());
		assertNotNull(resposta.getDescricao());
		assertNotNull(resposta.getModalidade());
		assertNotNull(resposta.getParcelas());
		assertNotNull(resposta.getValor());
		assertNotNull(resposta.getDataHora());

		SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
		String dataString = fmt.format(resposta.getDataHora().getTime());
		assertEquals("2015-11-12T10:10:31.471-02:00", dataString);

		assertNull(resposta.getNumeroCartao());
		assertNull(resposta.getStatusPagamento());

	}

	@Test
	public void testaCancelamentoNegado() {
		StringBuilder response = new StringBuilder();
		response.append("<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\">");
		response.append("<SOAP-ENV:Header />");
		response.append("<SOAP-ENV:Body>");
		response.append("<ns2:CancelamentoResponse xmlns:ns2=\"cancelamento.bandeirax.contrato.ws.desafiom4u\"");
		response.append("	xmlns:ns3=\"common.bandeirax.contrato.ws.desafiom4u\">");
		response.append("	<ns2:transacao>");
		response.append("		<ns2:tid>1</ns2:tid>");
		response.append("		<ns2:dados-pedido>");
		response.append("			<ns2:valor>100</ns2:valor>");
		response.append("			<ns2:dataHora>2015-11-12T10:10:31.471-02:00</ns2:dataHora>");
		response.append("			<ns2:descricao>teste</ns2:descricao>");
//		response.append("			<ns2:nsu>123445566</ns2:nsu>");
//		response.append("			<ns2:codigoAutorizacao>998766541</ns2:codigoAutorizacao>");
		response.append("			<ns2:statusCancelamento>NEGADO</ns2:statusCancelamento>");
		response.append("		</ns2:dados-pedido>");
		response.append("		<ns2:forma-pagamento>");
		response.append("			<ns3:modalidade>AVISTA</ns3:modalidade>");
		response.append("			<ns3:parcelas>3</ns3:parcelas>");
		response.append("		</ns2:forma-pagamento>");
		response.append("	</ns2:transacao>");
		response.append("</ns2:CancelamentoResponse>");
		response.append("	</SOAP-ENV:Body>");
		response.append("</SOAP-ENV:Envelope>");

		stubFor(post(urlEqualTo("/ws"))
				.withRequestBody(equalToXml(
						"<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\"><SOAP-ENV:Header/><SOAP-ENV:Body><ns4:CancelamentoRequest xmlns:ns2=\"autorizacao.bandeirax.contrato.ws.desafiom4u\" xmlns:ns3=\"common.bandeirax.contrato.ws.desafiom4u\" xmlns:ns4=\"cancelamento.bandeirax.contrato.ws.desafiom4u\" xmlns:ns5=\"consulta.bandeirax.contrato.ws.desafiom4u\"><ns4:transacao><ns2:tid>1</ns2:tid><ns2:dados-pedido><ns2:numeroCartao>1234567890</ns2:numeroCartao><ns2:valor>100</ns2:valor><ns2:dataHora>2015-11-12T10:10:31.471-02:00</ns2:dataHora><ns2:descricao>teste</ns2:descricao></ns2:dados-pedido><ns2:forma-pagamento><ns3:modalidade>AVISTA</ns3:modalidade><ns3:parcelas>3</ns3:parcelas></ns2:forma-pagamento></ns4:transacao></ns4:CancelamentoRequest></SOAP-ENV:Body></SOAP-ENV:Envelope>"))
				.willReturn(aResponse().withStatus(200).withHeader("Content-Type", "text/xml")
						.withBody(response.toString())));

		TransacaoVO resposta = bandeiraXadaptor.cancela(this.transacaoVOSucesso);
		assertNull(resposta.getCodAutorizacao());
		assertNull(resposta.getNsu());
		assertNotNull(resposta.getStatusCancelamento());
		assertNotNull(resposta.getTid());
		assertNotNull(resposta.getDescricao());
		assertNotNull(resposta.getModalidade());
		assertNotNull(resposta.getParcelas());
		assertNotNull(resposta.getValor());
		assertNotNull(resposta.getDataHora());

		SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
		String dataString = fmt.format(resposta.getDataHora().getTime());
		assertEquals("2015-11-12T10:10:31.471-02:00", dataString);

		assertNull(resposta.getNumeroCartao());
		assertNull(resposta.getStatusPagamento());

	}

	
	@Test
	public void testaConsultaSucesso() {
		StringBuilder response = new StringBuilder();

		response.append("<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\">");
		response.append("<SOAP-ENV:Header />");
		response.append("<SOAP-ENV:Body>");
		response.append("<ns2:ConsultaResponse xmlns:ns2=\"consulta.bandeirax.contrato.ws.desafiom4u\"");
		response.append("	xmlns:ns3=\"common.bandeirax.contrato.ws.desafiom4u\">");
		response.append("	<ns2:transacoes>");
		response.append("	<ns2:transacao>");
		response.append("		<ns2:tid>1</ns2:tid>");
		response.append("		<ns2:dados-pedido>");
		response.append("			<ns2:numeroCartao>1234567890</ns2:numeroCartao>");
		response.append("			<ns2:valor>100</ns2:valor>");
		response.append("			<ns2:dataHora>2015-11-12T10:10:31.471-02:00</ns2:dataHora>");
		response.append("			<ns2:descricao>teste</ns2:descricao>");
		response.append("			<ns2:nsu>123445566</ns2:nsu>");
		response.append("			<ns2:codigoAutorizacao>998766541</ns2:codigoAutorizacao>");
		response.append("			<ns2:statusCancelamento>AUTORIZADO</ns2:statusCancelamento>");
		response.append("		</ns2:dados-pedido>");
		response.append("		<ns2:forma-pagamento>");
		response.append("			<ns3:modalidade>AVISTA</ns3:modalidade>");
		response.append("			<ns3:parcelas>3</ns3:parcelas>");
		response.append("		</ns2:forma-pagamento>");
		response.append("	</ns2:transacao>");
		response.append("	<ns2:transacao>");
		response.append("		<ns2:tid>2</ns2:tid>");
		response.append("		<ns2:dados-pedido>");
		response.append("			<ns2:numeroCartao>0987654321</ns2:numeroCartao>");
		response.append("			<ns2:valor>100</ns2:valor>");
		response.append("			<ns2:dataHora>2015-11-12T10:10:31.471-02:00</ns2:dataHora>");
		response.append("			<ns2:descricao>teste</ns2:descricao>");
		response.append("			<ns2:nsu>123445566</ns2:nsu>");
		response.append("			<ns2:codigoAutorizacao>998766541</ns2:codigoAutorizacao>");
		response.append("			<ns2:statusPagamento>AUTORIZADA</ns2:statusPagamento>");
		response.append("		</ns2:dados-pedido>");
		response.append("		<ns2:forma-pagamento>");
		response.append("			<ns3:modalidade>AVISTA</ns3:modalidade>");
		response.append("			<ns3:parcelas>3</ns3:parcelas>");
		response.append("		</ns2:forma-pagamento>");
		response.append("	</ns2:transacao>");
		response.append("	</ns2:transacoes>");
		response.append("</ns2:ConsultaResponse>");
		response.append("	</SOAP-ENV:Body>");
		response.append("</SOAP-ENV:Envelope>");

		stubFor(post(urlEqualTo("/ws"))
				.withRequestBody(equalToXml(
						"<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\"><SOAP-ENV:Header/><SOAP-ENV:Body><ns5:ConsultaRequest xmlns:ns2=\"autorizacao.bandeirax.contrato.ws.desafiom4u\" xmlns:ns3=\"common.bandeirax.contrato.ws.desafiom4u\" xmlns:ns4=\"cancelamento.bandeirax.contrato.ws.desafiom4u\" xmlns:ns5=\"consulta.bandeirax.contrato.ws.desafiom4u\"><ns5:transacoes><ns5:tid>1</ns5:tid><ns5:tid>2</ns5:tid></ns5:transacoes></ns5:ConsultaRequest></SOAP-ENV:Body></SOAP-ENV:Envelope>"))
				.willReturn(aResponse().withStatus(200).withHeader("Content-Type", "text/xml")
						.withBody(response.toString())));

		List<String> lista = Arrays.asList("1", "2");
		List<TransacaoVO> resposta = bandeiraXadaptor.consulta(lista);

		for (TransacaoVO transacaoVO : resposta) {
			assertNotNull(transacaoVO.getCodAutorizacao());
			assertNotNull(transacaoVO.getNsu());
			assertNotNull(transacaoVO.getTid());
			assertNotNull(transacaoVO.getDescricao());
			assertNotNull(transacaoVO.getModalidade());
			assertNotNull(transacaoVO.getParcelas());
			assertNotNull(transacaoVO.getValor());
			assertNotNull(transacaoVO.getDataHora());
			assertNotNull(transacaoVO.getNumeroCartao());

			SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
			String dataString = fmt.format(transacaoVO.getDataHora().getTime());
			assertEquals("2015-11-12T10:10:31.471-02:00", dataString);

			if (transacaoVO.getStatusCancelamento() == null) {
				assertNull(transacaoVO.getStatusCancelamento());
				assertNotNull(transacaoVO.getStatusPagamento());
			} else {
				assertNotNull(transacaoVO.getStatusCancelamento());
				assertNull(transacaoVO.getStatusPagamento());
			}

		}
	}

	@Test(expected = NullPointerException.class)
	public void testaConsultaDadoInvalido() {

		bandeiraXadaptor.consulta(null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testaConsultaErroUnmarshalling() {
		StringBuilder response = new StringBuilder();

		response.append("<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\">");
		response.append("<SOAP-ENV:Header />");
		response.append("<SOAP-ENV:Body>");
		response.append("<ns2:ConsultaResponse xmlns:ns2=\"consulta.bandeirax.contrato.ws.desafiom4u\"");
		response.append("	xmlns:ns3=\"common.bandeirax.contrato.ws.desafiom4u\">");
		response.append("	<ns2:transacoes>");
		response.append("	<ns2:transacao>");
		response.append("		<ns2:tid>1</ns2:tid>");
		response.append("		<ns2:dados-pedido>");
		response.append("			<ns2:numeroCartao>1234567890</ns2:numeroCartao>");
		response.append("			<ns2:valor>100</ns2:valor>");
		response.append("			<ns2:dataHora>2015-11-12T10:10:31.471-02:00</ns2:dataHora>");
		response.append("			<ns2:descricao>teste</ns2:descricao>");
		response.append("			<ns2:nsu>123445566</ns2:nsu>");
		response.append("			<ns2:codigoAutorizacao>998766541</ns2:codigoAutorizacao>");
		response.append("			<ns2:statusCancelamento>AUTORIZADO</ns2:statusCancelamento>");
		response.append("		</ns2:dados-pedido>");
		response.append("		<ns2:forma-pagamento>");
		response.append("			");
		response.append("			<ns3:parcelas>3</ns3:parcelas>");
		response.append("		</ns2:forma-pagamento>");
		response.append("	</ns2:transacao>");
		response.append("	<ns2:transacao>");
		response.append("		<ns2:tid>2</ns2:tid>");
		response.append("		<ns2:dados-pedido>");
		response.append("			<ns2:numeroCartao>0987654321</ns2:numeroCartao>");
		response.append("			<ns2:valor>100</ns2:valor>");
		response.append("			<ns2:dataHora>2015-11-12T10:10:31.471-02:00</ns2:dataHora>");
		response.append("			<ns2:descricao>teste</ns2:descricao>");
		response.append("			<ns2:nsu>123445566</ns2:nsu>");
		response.append("			<ns2:codigoAutorizacao>998766541</ns2:codigoAutorizacao>");
		response.append("			<ns2:statusPagamento>AUTORIZADA</ns2:statusPagamento>");
		response.append("		</ns2:dados-pedido>");
		response.append("		<ns2:forma-pagamento>");
		response.append("			<ns3:modalidade>AVISTA</ns3:modalidade>");
		response.append("			<ns3:parcelas>3</ns3:parcelas>");
		response.append("		</ns2:forma-pagamento>");
		response.append("	</ns2:transacao>");
		response.append("	</ns2:transacoes>");
		response.append("</ns2:ConsultaResponse>");
		response.append("	</SOAP-ENV:Body>");
		response.append("</SOAP-ENV:Envelope>");
		stubFor(post(urlEqualTo("/ws")).willReturn(
				aResponse().withStatus(200).withHeader("Content-Type", "text/xml").withBody(response.toString())));

		bandeiraXadaptor.consulta(Arrays.asList("1", "2"));
	}

	@Test(expected = IllegalStateException.class)
	public void testaConsultaErroConectividade() {

		stubFor(post(urlEqualTo("/ws")).willReturn(aResponse().withStatus(404)));

		bandeiraXadaptor.consulta(Arrays.asList("1", "2"));
	}

	@Test
	public void testaAutorizacaoAssincronaSucesso() {

		stubFor(post(urlEqualTo("/ws"))
				.willReturn(aResponse().withStatus(202).withHeader("Content-Type", "text/xml").withBody("OK")));

		try {

			bandeiraXadaptor.autorizaASinc(this.transacaoVOSucesso);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Test(expected = IllegalArgumentException.class)
	public void testaAutorizacaoAssincErroMarshalling() {

		bandeiraXadaptor.autorizaASinc(this.transacaoVOErro);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testaAutorizacaoAssincErroUnmarshalling() {
		StringBuilder response = new StringBuilder();
		response.append("<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\">");
		response.append("<SOAP-ENV:Header />");
		response.append("<SOAP-ENV:Body>");
		response.append("<ns2:AutorizacaoResponse xmlns:ns2=\"autorizacao.bandeirax.contrato.ws.desafiom4u\"");
		response.append("	xmlns:ns3=\"common.bandeirax.contrato.ws.desafiom4u\">");
		response.append("	<ns2:transacao>");
		response.append("		<ns2:tid>1</ns2:tid>");
		response.append("		<ns2:dados-pedido>");
		response.append("			<ns2:valor>100</ns2:valor>");
		response.append("			<ns2:dataHora>2015-11-12T10:10:31.471-02:00</ns2:dataHora>");
		response.append("			<ns2:descricao>teste</ns2:descricao>");
		response.append("			<ns2:nsu>123445566</ns2:nsu>");
		response.append("			<ns2:codigoAutorizacao>998766541</ns2:codigoAutorizacao>");
		response.append("			<ns2:statusPagamento>AUTORIZADA</ns2:statusPagamento>");
		response.append("		</ns2:dados-pedido>");
		response.append("		<ns2:forma-pagamento>");
		response.append("			");
		response.append("			<ns3:parcelas>3</ns3:parcelas>");
		response.append("		</ns2:forma-pagamento>");
		response.append("	</ns2:transacao>");
		response.append("</ns2:AutorizacaoResponse>");
		response.append("	</SOAP-ENV:Body>");
		response.append("</SOAP-ENV:Envelope>");

		stubFor(post(urlEqualTo("/ws")).willReturn(
				aResponse().withStatus(200).withHeader("Content-Type", "text/xml").withBody(response.toString())));

		bandeiraXadaptor.autorizaSinc(this.transacaoVOSucesso);
	}

	@Test(expected = IllegalStateException.class)
	public void testaAutorizacaoAssincErroConectividade() {

		stubFor(post(urlEqualTo("/ws")).willReturn(aResponse().withStatus(404)));

		bandeiraXadaptor.autorizaSinc(this.transacaoVOSucesso);
	}

	@AfterClass
	public static void stopWireMock() {
		wireMock.stop();
		wireMock.shutdown();
	}

}
