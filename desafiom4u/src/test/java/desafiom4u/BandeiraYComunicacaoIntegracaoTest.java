package desafiom4u;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.github.tomakehurst.wiremock.junit.WireMockClassRule;

import desafiom4u.adaptor.bandeiray.BandeiraYAdaptor;
import desafiom4u.configuracao.BandeiraYConfiguracao;
import desafiom4u.vo.TransacaoVO;
import desafiom4u.ws.contrato.bandeirax.common.EnumModalidade;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = BandeiraYConfiguracao.class)

public class BandeiraYComunicacaoIntegracaoTest {

	@ClassRule
	public static WireMockClassRule wireMock = new WireMockClassRule(8089);

	@Rule
	public WireMockClassRule instanceRule = wireMock;

	TransacaoVO transacaoVO;

	@Autowired
	BandeiraYAdaptor bandeiraYadaptor;

	private TransacaoVO transacaoVOErro;

	@Before
	public void setUp() {
		SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
		Calendar c = Calendar.getInstance();
		try {
			c.setTime(fmt.parse("2011-12-05T16:01:28.655-02:00"));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		transacaoVO = new TransacaoVO("1", null, null, c, "teste", 100, null, null, EnumModalidade.AVISTA, 3,
				"1234567890");
		transacaoVOErro = new TransacaoVO(null, null, null, c, "teste", 100, null, null, EnumModalidade.AVISTA, 3,
				"1234567890");
	}

	@Test
	public void autorizacao() {
		StringBuilder response = new StringBuilder();

		response.append("	{");
		response.append("	    \"transacao\": {");
		response.append("	        \"tid\": \"1\",");
		response.append("	        \"dados-pedido\": {");
		response.append("        \"valor\": \"100\",");
		response.append("        \"data-hora\": \"2011-12-05T16:01:28.655-02:00\",");
		response.append("        \"descricao\": \"teste\",");
		response.append("        \"nsu\": \"123445566\",");
		response.append("        \"codigoAutorizacao\": \"998766541\",");
		response.append("        \"status\": \"AUTORIZADO\"");
		response.append("         },");
		response.append("         \"forma-pagamento\": {");
		response.append("         \"tipo\": \"AVISTA\",");
		response.append("         \"quantidade\": \"1\"");
		response.append("             }");
		response.append("         }");
		response.append("         }");

		stubFor(post(urlEqualTo("/rest"))
				.withRequestBody(equalToJson(
						"{\"transacao\":{\"tid\":\"1\",\"forma-pagamento\":{\"tipo\":\"AVISTA\",\"quantidade\":3},\"dados-pedido\":{\"numeroCartao\":\"1234567890\",\"valor\":100,\"data-hora\":\"2011-12-05T16:01:28.655-02:00\",\"descricao\":\"teste\"}}}"))
				.willReturn(aResponse().withStatus(200).withHeader("Content-Type", "application/json")
						.withBody(response.toString())));

		
			TransacaoVO resposta = bandeiraYadaptor.autorizaSinc(this.transacaoVO);
			assertNotNull(resposta.getCodAutorizacao());
			assertNotNull(resposta.getNsu());
			assertNotNull(resposta.getStatusPagamento());
			assertNotNull(resposta.getTid());
			assertNotNull(resposta.getDescricao());
			assertNotNull(resposta.getModalidade());
			assertNotNull(resposta.getParcelas());
			assertNotNull(resposta.getValor());
			assertNotNull(resposta.getDataHora());

			SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
			String dataString = fmt.format(resposta.getDataHora().getTime());
			assertEquals("2011-12-05T16:01:28.655-02:00", dataString);

			assertNull(resposta.getNumeroCartao());
			assertNull(resposta.getStatusCancelamento());

	}
	
	@Test
	public void autorizacaoNegada() {
		StringBuilder response = new StringBuilder();

		response.append("	{");
		response.append("	    \"transacao\": {");
		response.append("	        \"tid\": \"1\",");
		response.append("	        \"dados-pedido\": {");
		response.append("        \"valor\": \"100\",");
		response.append("        \"data-hora\": \"2011-12-05T16:01:28.655-02:00\",");
		response.append("        \"descricao\": \"teste\",");
//		response.append("        \"nsu\": \"123445566\",");
//		response.append("        \"codigoAutorizacao\": \"998766541\",");
		response.append("        \"status\": \"NAO AUTORIZADO\"");
		response.append("         },");
		response.append("         \"forma-pagamento\": {");
		response.append("         \"tipo\": \"AVISTA\",");
		response.append("         \"quantidade\": \"1\"");
		response.append("             }");
		response.append("         }");
		response.append("         }");

		stubFor(post(urlEqualTo("/rest"))
				.withRequestBody(equalToJson(
						"{\"transacao\":{\"tid\":\"1\",\"forma-pagamento\":{\"tipo\":\"AVISTA\",\"quantidade\":3},\"dados-pedido\":{\"numeroCartao\":\"1234567890\",\"valor\":100,\"data-hora\":\"2011-12-05T16:01:28.655-02:00\",\"descricao\":\"teste\"}}}"))
				.willReturn(aResponse().withStatus(200).withHeader("Content-Type", "application/json")
						.withBody(response.toString())));

		
			TransacaoVO resposta = bandeiraYadaptor.autorizaSinc(this.transacaoVO);
			assertNull(resposta.getCodAutorizacao());
			assertNull(resposta.getNsu());
			assertNotNull(resposta.getStatusPagamento());
			assertNotNull(resposta.getTid());
			assertNotNull(resposta.getDescricao());
			assertNotNull(resposta.getModalidade());
			assertNotNull(resposta.getParcelas());
			assertNotNull(resposta.getValor());
			assertNotNull(resposta.getDataHora());

			SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
			String dataString = fmt.format(resposta.getDataHora().getTime());
			assertEquals("2011-12-05T16:01:28.655-02:00", dataString);

			assertNull(resposta.getNumeroCartao());
			assertNull(resposta.getStatusCancelamento());

	}


	@Test(expected = IllegalStateException.class)
	public void testaAutorizacaoInputsInvalidos() {

		stubFor(post(urlEqualTo("/rest"))
				.withRequestBody(equalToJson(
						"{\"transacao\":{\"forma-pagamento\":{\"tipo\":\"AVISTA\",\"quantidade\":3},\"dados-pedido\":{\"numeroCartao\":\"1234567890\",\"valor\":100,\"data-hora\":\"2011-12-05T16:01:28.655-02:00\",\"descricao\":\"teste\"}}}"))
				.willReturn(aResponse().withStatus(400)));

		bandeiraYadaptor.autorizaSinc(this.transacaoVOErro);

	}

	@Test(expected = IllegalArgumentException.class)
	public void testaAutorizacaoOutputInvalido() {
		StringBuilder response = new StringBuilder();

		response.append("	{");
		response.append("	    \"transacao\": {");
		response.append("	        \"tid\": \"1\",");
		response.append("	        \"dados-pedido\": {");
		response.append("        \"valor\": \"100\",");
		response.append("        \"data-hora\": \"2011-12-05T16:01:28.655-02:00\",");
		response.append("        \"descricao\": \"teste\",");
		response.append("        ");
		response.append("        \"codigoAutorizacao\": \"998766541\",");
		response.append("        \"status\": \"AUTORIZADO\"");
		response.append("         },");
		response.append("         \"forma-pagamento\": {");
		response.append("         \"tipo\": \"AVISTA\",");
		response.append("         \"quantidade\": \"1\"");
		response.append("             }");
		response.append("         }");
		response.append("         }");

		stubFor(post(urlEqualTo("/rest"))
				.withRequestBody(equalToJson(
						"{\"transacao\":{\"tid\":\"1\",\"forma-pagamento\":{\"tipo\":\"AVISTA\",\"quantidade\":3},\"dados-pedido\":{\"numeroCartao\":\"1234567890\",\"valor\":100,\"data-hora\":\"2011-12-05T16:01:28.655-02:00\",\"descricao\":\"teste\"}}}"))
				.willReturn(aResponse().withStatus(200).withHeader("Content-Type", "application/json")
						.withBody(response.toString())));

		bandeiraYadaptor.autorizaSinc(this.transacaoVO);

	}

	@Test(expected = IllegalStateException.class)
	public void testaAutorizacaoErroConexao() {

		stubFor(post(urlEqualTo("/rest"))

				.willReturn(aResponse().withStatus(404)));
		bandeiraYadaptor.autorizaSinc(this.transacaoVO);
	}

	@Test
	public void testaCancelamentoSucesso() {
		StringBuilder response = new StringBuilder();

		response.append("	{");
		response.append("	    \"transacao\": {");
		response.append("	        \"tid\": \"1\",");
		response.append("	        \"dados-pedido\": {");
		response.append("        \"valor\": \"100\",");
		response.append("        \"data-hora\": \"2011-12-05T16:01:28.655-02:00\",");
		response.append("        \"descricao\": \"teste\",");
		response.append("        \"nsu\": \"123445566\",");
		response.append("        \"codigoAutorizacao\": \"998766541\",");
		response.append("        \"statusCancelamento\": \"AUTORIZADO\"");
		response.append("         },");
		response.append("         \"forma-pagamento\": {");
		response.append("         \"modalidade\": \"AVISTA\",");
		response.append("         \"parcelas\": \"1\"");
		response.append("             }");
		response.append("         }");
		response.append("         }");

		stubFor(post(urlEqualTo("/rest/cancelamento"))
				.withRequestBody(equalToJson(
						"{\"transacao\":{\"tid\":\"1\",\"forma-pagamento\":{\"tipo\":\"AVISTA\",\"quantidade\":3},\"dados-pedido\":{\"numeroCartao\":\"1234567890\",\"valor\":100,\"data-hora\":\"2011-12-05T16:01:28.655-02:00\",\"descricao\":\"teste\"}}}"))
				.willReturn(aResponse().withStatus(200).withHeader("Content-Type", "application/json")
						.withBody(response.toString())));

		
			TransacaoVO resposta = bandeiraYadaptor.cancela(this.transacaoVO);
			assertNotNull(resposta.getCodAutorizacao());
			assertNotNull(resposta.getNsu());
			assertNotNull(resposta.getStatusCancelamento());
			assertNotNull(resposta.getTid());
			assertNotNull(resposta.getDescricao());
			assertNotNull(resposta.getModalidade());
			assertNotNull(resposta.getParcelas());
			assertNotNull(resposta.getValor());
			assertNotNull(resposta.getDataHora());

			SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
			String dataString = fmt.format(resposta.getDataHora().getTime());
			assertEquals("2011-12-05T16:01:28.655-02:00", dataString);

			assertNull(resposta.getNumeroCartao());
			assertNull(resposta.getStatusPagamento());

	}
	
	@Test
	public void testaCancelamentoNegado() {
		StringBuilder response = new StringBuilder();

		response.append("	{");
		response.append("	    \"transacao\": {");
		response.append("	        \"tid\": \"1\",");
		response.append("	        \"dados-pedido\": {");
		response.append("        \"valor\": \"100\",");
		response.append("        \"data-hora\": \"2011-12-05T16:01:28.655-02:00\",");
		response.append("        \"descricao\": \"teste\",");
//		response.append("        \"nsu\": \"123445566\",");
//		response.append("        \"codigoAutorizacao\": \"998766541\",");
		response.append("        \"statusCancelamento\": \"NEGADO\"");
		response.append("         },");
		response.append("         \"forma-pagamento\": {");
		response.append("         \"modalidade\": \"AVISTA\",");
		response.append("         \"parcelas\": \"1\"");
		response.append("             }");
		response.append("         }");
		response.append("         }");

		stubFor(post(urlEqualTo("/rest/cancelamento"))
				.withRequestBody(equalToJson(
						"{\"transacao\":{\"tid\":\"1\",\"forma-pagamento\":{\"tipo\":\"AVISTA\",\"quantidade\":3},\"dados-pedido\":{\"numeroCartao\":\"1234567890\",\"valor\":100,\"data-hora\":\"2011-12-05T16:01:28.655-02:00\",\"descricao\":\"teste\"}}}"))
				.willReturn(aResponse().withStatus(200).withHeader("Content-Type", "application/json")
						.withBody(response.toString())));

		
			TransacaoVO resposta = bandeiraYadaptor.cancela(this.transacaoVO);
			assertNull(resposta.getCodAutorizacao());
			assertNull(resposta.getNsu());
			assertNotNull(resposta.getStatusCancelamento());
			assertNotNull(resposta.getTid());
			assertNotNull(resposta.getDescricao());
			assertNotNull(resposta.getModalidade());
			assertNotNull(resposta.getParcelas());
			assertNotNull(resposta.getValor());
			assertNotNull(resposta.getDataHora());

			SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
			String dataString = fmt.format(resposta.getDataHora().getTime());
			assertEquals("2011-12-05T16:01:28.655-02:00", dataString);

			assertNull(resposta.getNumeroCartao());
			assertNull(resposta.getStatusPagamento());

	}


	@Test(expected = IllegalStateException.class)
	public void testaCancelamentoInputsInvalidos() {

		stubFor(post(urlEqualTo("/rest"))
				.withRequestBody(equalToJson(
						"{\"transacao\":{\"forma-pagamento\":{\"tipo\":\"AVISTA\",\"quantidade\":3},\"dados-pedido\":{\"numeroCartao\":\"1234567890\",\"valor\":100,\"data-hora\":\"2011-12-05T16:01:28.655-02:00\",\"descricao\":\"teste\"}}}"))
				.willReturn(aResponse().withStatus(400)));

		bandeiraYadaptor.cancela(this.transacaoVOErro);

	}

	@Test(expected = IllegalArgumentException.class)
	public void testaCancelamentoOutputInvalido() {
		StringBuilder response = new StringBuilder();

		response.append("	{");
		response.append("	    \"transacao\": {");
		response.append("	        \"tid\": \"1\",");
		response.append("	        \"dados-pedido\": {");
		response.append("        \"valor\": \"100\",");
		response.append("        \"data-hora\": \"2011-12-05T16:01:28.655-02:00\",");
		response.append("        \"descricao\": \"teste\",");
		response.append("        ");
		response.append("        \"codigoAutorizacao\": \"998766541\",");
		response.append("        \"statusCancelamento\": \"AUTORIZADO\"");
		response.append("         },");
		response.append("         \"forma-pagamento\": {");
		response.append("         \"modalidade\": \"AVISTA\",");
		response.append("         \"parcelas\": \"1\"");
		response.append("             }");
		response.append("         }");
		response.append("         }");

		stubFor(post(urlEqualTo("/rest/cancelamento"))
				.withRequestBody(equalToJson(
						"{\"transacao\":{\"tid\":\"1\",\"forma-pagamento\":{\"tipo\":\"AVISTA\",\"quantidade\":3},\"dados-pedido\":{\"numeroCartao\":\"1234567890\",\"valor\":100,\"data-hora\":\"2011-12-05T16:01:28.655-02:00\",\"descricao\":\"teste\"}}}"))
				.willReturn(aResponse().withStatus(200).withHeader("Content-Type", "application/json")
						.withBody(response.toString())));

		bandeiraYadaptor.cancela(this.transacaoVO);

	}

	@Test(expected = IllegalStateException.class)
	public void testaCancelamentoErroConexao() {

		stubFor(post(urlEqualTo("/rest"))
				.willReturn(aResponse().withStatus(404)));
		
		bandeiraYadaptor.cancela(this.transacaoVO);
	}
	
	@Test
	public void testeConsultaSucesso() {
		stubFor(get(urlMatching("/rest/.*"))
				.willReturn(aResponse().withStatus(200).withHeader("Content-Type", "application/json").withBody(
						"[{\"tid\":\"1\",\"dados-pedido\":{\"numeroCartao\":\"1234567890\",\"valor\":\"1000\",\"data-hora\":\"2011-12-05T16:01:28.655-02:00\",\"descricao\":\"LojadeConveniência\",\"nsu\":\"123445566\",\"codigoAutorizacao\":\"998766541\",\"status\":\"AUTORIZADO\"},\"forma-pagamento\":{\"modalidade\":\"AVISTA\",\"parcelas\":\"1\"}},{\"tid\":\"2\",\"dados-pedido\":{\"numeroCartao\":\"1130006436\",\"valor\":\"1000\",\"data-hora\":\"2011-12-05T16:01:28.655-02:00\",\"descricao\":\"LojadeConveniência\",\"nsu\":\"123445566\",\"codigoAutorizacao\":\"998766541\",\"status\":\"AUTORIZADO\"},\"forma-pagamento\":{\"modalidade\":\"AVISTA\",\"parcelas\":\"1\"}}]")));
		
			List<String> lista = Arrays.asList("1", "2");
			List<TransacaoVO> resposta = bandeiraYadaptor.consulta(lista);	
			for (TransacaoVO transacaoVO : resposta) {
				assertNotNull(transacaoVO.getCodAutorizacao());
				assertNotNull(transacaoVO.getNsu());
				assertNotNull(transacaoVO.getTid());
				assertNotNull(transacaoVO.getDescricao());
				assertNotNull(transacaoVO.getModalidade());
				assertNotNull(transacaoVO.getParcelas());
				assertNotNull(transacaoVO.getValor());
				assertNotNull(transacaoVO.getDataHora());
				assertNotNull(transacaoVO.getNumeroCartao());

				SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
				String dataString = fmt.format(transacaoVO.getDataHora().getTime());
				
				assertEquals("2011-12-05T16:01:28.655-02:00", dataString);

				if (transacaoVO.getStatusCancelamento() == null) {
					assertNull(transacaoVO.getStatusCancelamento());
					assertNotNull(transacaoVO.getStatusPagamento());
				} else {
					assertNotNull(transacaoVO.getStatusCancelamento());
					assertNull(transacaoVO.getStatusPagamento());
				}

			}
	}
	@Test(expected = IllegalStateException.class)
	public void testaConsultaInputInvalido() {	
		stubFor(get(urlMatching("/rest/"))
				.willReturn(aResponse().withStatus(400)));		
			
			bandeiraYadaptor.consulta(null);			
		
	}
	@Test(expected = IllegalArgumentException.class)
	public void testaConsultaOutputInvalido() {
		stubFor(get(urlMatching("/rest/.*"))
				.willReturn(aResponse().withStatus(200).withHeader("Content-Type", "application/json").withBody(
						"[{\"tid\":\"1\",\"dados-pedido\":{\"numero_cartao\":\"1130006436\",\"valor\":\"1000\",\"data-hora\":\"2011-12-05T16:01:28.655-02:00\",\"descricao\":\"LojadeConveniência\",\"nsu\":\"123445566\",\"codigoAutorizacao\":\"998766541\",\"status\":\"AUTORIZADO\"},\"forma-pagamento\":{\"modalidade\":\"AVISTA\",\"parcelas\":\"1\"}},{\"tid\":\"2\",\"dados-pedido\":{\"numero_cartao\":\"1130006436\",\"valor\":\"1000\",\"data-hora\":\"2011-12-05T16:01:28.655-02:00\",\"descricao\":\"LojadeConveniência\",\"nsu\":\"123445566\",\"codigoAutorizacao\":\"998766541\",\"statusCancelamento\":\"AUTORIZADO\"},\"forma-pagamento\":{\"modalidade\":\"AVISTA\",\"parcelas\":\"1\"}}]")));
		
			List<String> lista = Arrays.asList("1", "2");
			bandeiraYadaptor.consulta(lista);			
		
	}
	@Test(expected = IllegalStateException.class)
	public void testaConsultaErroConexao() {
		stubFor(get(urlMatching("/rest/.*"))
				.willReturn(aResponse().withStatus(404)));		
		
			List<String> lista = Arrays.asList("1", "2");
			bandeiraYadaptor.consulta(lista);			
		
	}

	@AfterClass
	public static void stopWireMock() {
		wireMock.stop();
		wireMock.shutdown();
	}

}
