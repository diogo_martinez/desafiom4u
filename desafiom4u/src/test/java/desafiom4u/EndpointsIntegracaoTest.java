package desafiom4u;

import static org.mockito.Mockito.doNothing;

import static org.mockito.Mockito.when;
import static org.springframework.ws.test.server.RequestCreators.withPayload;

import java.util.Calendar;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.ws.test.server.MockWebServiceClient;
import org.springframework.ws.test.server.ResponseMatchers;
import org.springframework.xml.transform.StringSource;

import desafiom4u.configuracao.Main;
import desafiom4u.repository.TransacaoRepository;
import desafiom4u.service.TransacaoService;
import desafiom4u.vo.TransacaoVO;
import desafiom4u.ws.contrato.bandeirax.common.EnumModalidade;
import desafiom4u.ws.endpoint.bandeirax.autorizacao.RespostaAutorizacaoEndPoint;
import desafiom4u.ws.endpoint.bandeiray.autorizacao.RespostaAutorizacaoRestfulEndPoint;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes =  Main.class)
@WebAppConfiguration
@DirtiesContext(classMode = ClassMode.AFTER_CLASS)
public class EndpointsIntegracaoTest {


	@Autowired
	WebApplicationContext wac;
	@Autowired
	MockHttpSession session;
	@Autowired
	MockHttpServletRequest request;
	
	private MockMvc mockMvc;
	
	@Autowired
    private ApplicationContext applicationContext;

    private MockWebServiceClient mockClient;
    
    @Autowired    
    @InjectMocks   
    RespostaAutorizacaoEndPoint endpoint;
    
    @Autowired    
    @InjectMocks
    RespostaAutorizacaoRestfulEndPoint restEndpoint;
    
    @Autowired
    @Spy
	TransacaoService transacaoService;
    
    @Autowired
    TransacaoRepository repositorio;

	@Mock
	TransacaoVO transacaoVO;
	

	@Before
	public void setup() {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
		mockClient = MockWebServiceClient.createClient(applicationContext);
		MockitoAnnotations.initMocks(this);
		repositorio.inserirTransacao(new TransacaoVO("12345",null,null,Calendar.getInstance(),"teste",100,"AUTORIZADO",null,EnumModalidade.PARCELADO_ADM,3,"1234567890"));
	}

	@Test
	public void testaEndpointRestSucesso() {

		doNothing().when(transacaoService).atualizaAutorizacao(transacaoVO);

		String json = "{ \"transacao\": { \"tid\": \"12345\", \"dados-pedido\": { \"valor\": \"100\", \"data-hora\": \"2011-12-05T16:01:28.655-02:00\", \"descricao\": \"Loja de Conveniência\", \"nsu\": \"123445566\", \"codigoAutorizacao\": \"998766541\", \"status\": \"AUTORIZADO\" }, \"forma-pagamento\": { \"tipo\": \"AVISTA\", \"quantidade\":\"1\" } } }";

		try {
			mockMvc.perform(MockMvcRequestBuilders.post("http://localhost:8080/desafio/rest/respostaAutorizacao")
					.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).content(json))
					.andExpect(MockMvcResultMatchers.status().isOk());
		} catch (Exception e) {

			e.printStackTrace();
		}
	}
	
	@Test
	public void testaEndpointRestNegada() {

		doNothing().when(transacaoService).atualizaAutorizacao(transacaoVO);

		String json = "{ \"transacao\": { \"tid\": \"12345\", \"dados-pedido\": { \"valor\": \"100\", \"data-hora\": \"2011-12-05T16:01:28.655-02:00\", \"descricao\": \"Loja de Conveniência\",  \"status\": \"NAO AUTORIZADO\" }, \"forma-pagamento\": { \"tipo\": \"AVISTA\", \"quantidade\":\"1\" } } }";

		try {
			mockMvc.perform(MockMvcRequestBuilders.post("http://localhost:8080/desafio/rest/respostaAutorizacao")
					.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).content(json))
					.andExpect(MockMvcResultMatchers.status().isOk());
		} catch (Exception e) {

			e.printStackTrace();
		}
	}

	
	@Test
	public void testaEndpointRestIdTransacaoInvalido() {		

		String json = "{ \"transacao\": { \"tid\": \"10017348980735271001\", \"dados-pedido\": { \"valor\": \"100\", \"data-hora\": \"2011-12-05T16:01:28.655-02:00\", \"descricao\": \"Loja de Conveniência\", \"nsu\": \"123445566\", \"codigoAutorizacao\": \"998766541\", \"status\": \"AUTORIZADO\" }, \"forma-pagamento\": { \"tipo\": \"AVISTA\", \"quantidade\":\"1\" } } }";

		try {
			
			when(transacaoService).thenCallRealMethod();
			mockMvc.perform(MockMvcRequestBuilders.post("http://localhost:8080/desafio/rest/respostaAutorizacao")
					.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).content(json))
					.andExpect(MockMvcResultMatchers.status().is(400));
		} catch (Exception e) {

			e.printStackTrace();
		}
	}

	@Test
	public void testaEndpointRestDadosInvalidos() {

		
		String json = "{ \"transacao\": { \"tid\": \"10017348980735271001\", \"dados-pedido\": { \"valor\": \"100\", \"data-hora\": \"2011-12-05T16:01:28.655-02:00\", \"descricao\": \"Loja de Conveniência\",  \"codigoAutorizacao\": \"998766541\", \"status\": \"AUTORIZADO\" }, \"forma-pagamento\": { \"tipo\": \"AVISTA\", \"quantidade\":\"1\" } } }";

		try {
			mockMvc.perform(MockMvcRequestBuilders.post("http://localhost:8080/desafio/rest/respostaAutorizacao")
					.contentType(MediaType.APPLICATION_JSON_VALUE).accept(MediaType.APPLICATION_JSON_VALUE)
					.content(json)).andExpect(MockMvcResultMatchers.status().is(400));
		} catch (Exception e) {

			e.printStackTrace();
		}
	}

	@Test
	public void testaEndpointSOAPIdInvalido() {
		try {
			
			
			String xml = "<sch:RespostaAutorizacaoRequest xmlns:sch=\"autorizacao.bandeirax.contrato.endpoint.ws.desafiom4u\"> <sch:transacao> <sch:tid>1</sch:tid> <sch:dados-pedido> <sch:valor>100</sch:valor> <sch:dataHora>2015-11-12T10:10:31.471-02:00</sch:dataHora> <sch:descricao>teste</sch:descricao> <sch:nsu>123445566</sch:nsu> <sch:codigoAutorizacao>998766541</sch:codigoAutorizacao> <sch:statusPagamento>AUTORIZADA</sch:statusPagamento> </sch:dados-pedido> <sch:forma-pagamento> <sch:modalidade>AVISTA</sch:modalidade> <sch:parcelas>3</sch:parcelas> </sch:forma-pagamento> </sch:transacao> </sch:RespostaAutorizacaoRequest>";
			mockClient.sendRequest(withPayload(new StringSource(xml))).andExpect(ResponseMatchers.serverOrReceiverFault());
		} catch (Exception e) {

			e.printStackTrace();
		}
	}
	
	@Test
	public void testaEndpointSOAPDadosInvalidos() {
		try {
			
			String xml = "<sch:RespostaAutorizacaoRequest xmlns:sch=\"autorizacao.bandeirax.contrato.endpoint.ws.desafiom4u\"> <sch:transacao> <sch:tid>1</sch:tid> <sch:dados-pedido>  <sch:dataHora>2015-11-12T10:10:31.471-02:00</sch:dataHora> <sch:descricao>teste</sch:descricao> <sch:nsu>123445566</sch:nsu> <sch:codigoAutorizacao>998766541</sch:codigoAutorizacao> <sch:statusPagamento>AUTORIZADA</sch:statusPagamento> </sch:dados-pedido> <sch:forma-pagamento> <sch:modalidade>AVISTA</sch:modalidade> <sch:parcelas>3</sch:parcelas> </sch:forma-pagamento> </sch:transacao> </sch:RespostaAutorizacaoRequest>";
			mockClient.sendRequest(withPayload(new StringSource(xml))).andExpect(ResponseMatchers.clientOrSenderFault());
		} catch (Exception e) {

			e.printStackTrace();
		}
	}
	
	@Test
	public void testaEndpointSOAPSucesso() {
		try {
			
			String xml = "<sch:RespostaAutorizacaoRequest xmlns:sch=\"autorizacao.bandeirax.contrato.endpoint.ws.desafiom4u\"> <sch:transacao> <sch:tid>12345</sch:tid> <sch:dados-pedido> <sch:valor>100</sch:valor> <sch:dataHora>2015-11-12T10:10:31.471-02:00</sch:dataHora> <sch:descricao>teste</sch:descricao> <sch:nsu>123445566</sch:nsu> <sch:codigoAutorizacao>998766541</sch:codigoAutorizacao> <sch:statusPagamento>AUTORIZADA</sch:statusPagamento> </sch:dados-pedido> <sch:forma-pagamento> <sch:modalidade>AVISTA</sch:modalidade> <sch:parcelas>3</sch:parcelas> </sch:forma-pagamento> </sch:transacao> </sch:RespostaAutorizacaoRequest>";
			mockClient.sendRequest(withPayload(new StringSource(xml))).andExpect(ResponseMatchers.noFault());
		} catch (Exception e) {

			e.printStackTrace();
		}
	}
	
	@Test
	public void testaEndpointSOAPNegada() {
		try {
			
			String xml = "<sch:RespostaAutorizacaoRequest xmlns:sch=\"autorizacao.bandeirax.contrato.endpoint.ws.desafiom4u\"> <sch:transacao> <sch:tid>12345</sch:tid> <sch:dados-pedido> <sch:valor>100</sch:valor> <sch:dataHora>2015-11-12T10:10:31.471-02:00</sch:dataHora> <sch:descricao>teste</sch:descricao><sch:statusPagamento>NAO AUTORIZADA</sch:statusPagamento> </sch:dados-pedido> <sch:forma-pagamento> <sch:modalidade>AVISTA</sch:modalidade> <sch:parcelas>3</sch:parcelas> </sch:forma-pagamento> </sch:transacao> </sch:RespostaAutorizacaoRequest>";
			mockClient.sendRequest(withPayload(new StringSource(xml))).andExpect(ResponseMatchers.noFault());
		} catch (Exception e) {

			e.printStackTrace();
		}
	}


}
