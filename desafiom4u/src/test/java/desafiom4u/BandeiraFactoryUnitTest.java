package desafiom4u;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import desafiom4u.adaptor.BandeiraAdaptor;
import desafiom4u.adaptor.bandeirax.BandeiraXAdaptor;
import desafiom4u.adaptor.bandeiray.BandeiraYAdaptor;
import desafiom4u.factory.BandeiraFactoryMethod;

public class BandeiraFactoryUnitTest {
	
	
	@InjectMocks
	BandeiraFactoryMethod factory;
	@Mock
	BandeiraYAdaptor adaptorY = new BandeiraYAdaptor();
	@Mock
	BandeiraXAdaptor adaptorX = new BandeiraXAdaptor();
	
	@Before
	public void setUp(){
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void testaCriacaoBandeiraX(){
		BandeiraAdaptor b = factory.createBandeira('X');
		assertTrue(b instanceof BandeiraXAdaptor);
		
	}
	@Test
	public void testaCriacaoBandeiraY(){
		BandeiraAdaptor b = factory.createBandeira('Y');
		assertTrue(b instanceof BandeiraYAdaptor);
		
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testaErroCriacaoBandeira(){
		factory.createBandeira('Z');
		
		
	}

}
